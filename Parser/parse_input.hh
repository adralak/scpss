#ifndef PARSE_INPUT_HH
#define PARSE_INPUT_HH

#include <fstream>
#include <vector>
#include <functional>

#include <CGAL/Combinatorial_map.h>
#include <CGAL/Cell_attribute.h>

#include "../Tools/tools.hh"

namespace SCPSS
{

class Input_Parser : public Tools
{
public:
	typedef std::ifstream ifstream;

	struct Hash_edge
	{
		std::size_t operator()(const Comb_edge& e) const
		{
			std::size_t hash_a(std::hash<unsigned>{}(e.a)),
				hash_b(std::hash<unsigned>{}(e.b));

			if(e.a < e.b)
				return hash_a ^ (hash_b << 1);
			else
				return hash_b ^ (hash_a << 1);
		}
	};

	struct Eq_edge
	{
		bool operator()(const Comb_edge& e1, const Comb_edge& e2) const
		{
			return (e1.a == e2.a && e1.b == e2.b)
				|| (e1.a == e2.b && e1.b == e2.a);
		}
	};

	typedef std::unordered_map<Comb_edge, Dart_handle, Hash_edge, Eq_edge>
	Edge_map;
	typedef Edge_map::iterator Edge_map_iterator;
	
	Input_Parser();
	
	Input_Parser(ifstream& input);

	Input_Parser(unsigned g, unsigned n, const Comb_edges& edges,
		     const std::string& filename);

private:
	Dart_handle find_edge_to_sew(const Dart_handle& ab_abd,
				     const unsigned long p,
				     const unsigned long q,
				     const unsigned long r);

	void add_last_vertex_info(const Dart_handle& abc,
				  const unsigned long a, const unsigned long b,
				  const unsigned long c);

	void add_generic_info_write_to_file(std::ofstream& f);

	void sew_boundary(const Boundary& bd);

	void sew_boundary(const Boundary& bd, ifstream& input);

	void insert_edge_in_poly(Boundary& bd, const Comb_edge& e);

	void find_in_orbit(const Dart_handle bd_a, const unsigned long b,
			   Dart_handle& actual_a, Dart_handle& actual_b);

	void sew_internal_edge(Edge_map& all_edges, Dart_handle edge,
			       const unsigned long a, const unsigned long b);
};

}


#endif
