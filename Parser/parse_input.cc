#include "parse_input.hh"

namespace SCPSS
{

typedef std::ifstream ifstream;
typedef Input_Parser::Dart_handle Dart_handle;		

Input_Parser::Input_Parser()
{
}

Input_Parser::Input_Parser(ifstream& input)
{
	bool gluing_table;

        // Get genus and number of punctures
	input >> g >> n >> gluing_table >> cubic_roots;

	// Make sure the Euler characteristic is negative
	if(2 * g + n <= 2)
	{
		std::cout << "Surface has positive Euler characteristic!"
			  << std::endl;

		return;
	}
	// Make sure the surface is punctured
	else if(n <= 0)
	{
		std::cout << "Surface has no punctures!" << std::endl;

		return;
	}
	
	// Coords dummy
	Coord ab, bc, ca, t;
	
	// Numbers: either vertices or iterators
	unsigned long a, b, c, i;

	// Number of sides of the fundamental polygon
	N = 4 * g + 2 * n - 2;
	
	Dart_handle abc;

	Edge_map all_edges;

	// There are N - 2 triangles
	for(i = 0; i < N - 2; ++i)
	{
		// Get the input
		input >> a >> b >> c >> ab >> bc >> ca >> t;

		// Create a triangle
		abc = cm.make_combinatorial_polygon(3);

		cm.set_attribute<0>(abc, cm.create_attribute<0>(a));
		cm.set_attribute<0>(cm.beta(abc, 1),
				    cm.create_attribute<0>(b));
		cm.set_attribute<0>(cm.beta(abc, 0),
				    cm.create_attribute<0>(c));

		// Assign the A-coordinates
		cm.info(abc).a = ab;
		cm.info(cm.beta(abc, 1)).a = bc;
		cm.info(cm.beta(abc, 0)).a = ca;

		sew_internal_edge(all_edges, abc, a, b);
		sew_internal_edge(all_edges, cm.beta(abc, 1), b, c);
		sew_internal_edge(all_edges, cm.beta(abc, 0), c, a);
		
		cm.set_attribute<2>(abc, cm.create_attribute<2>(t));
	}

	// Get the sides of the polygon
	Boundary bd(N);

	for(Edge_iterator
		    it(cm.one_dart_per_cell<1>().begin()),
		    itend(cm.one_dart_per_cell<1>().end());
	    it != itend; ++it)
	{
		if(cm.is_free<2>(it))
			bd.at(cm.info<0>(it)) = it;
	}

	if(!gluing_table)
		sew_boundary(bd);
	else
		sew_boundary(bd, input);
}

Input_Parser::Input_Parser(unsigned g, unsigned n, const Comb_edges& edges,
			   const std::string& filename)
{
	this->g = g;
	this->n = n;
	N = 4 * g + 2 * n - 2;

	cm.make_combinatorial_polygon(N);

	Vertex_iterator v_it(cm.one_dart_per_cell<0>().begin()),
		v_itend(cm.one_dart_per_cell<0>().end());

	unsigned long i;

	Vertices v;

	for(i = 0; v_it != v_itend; ++v_it, ++i)
	{
		cm.set_attribute<0>(v_it, cm.create_attribute<0>(i));
		v.push_back(v_it);
	}

	Boundary bd(v);
	
	for(const Comb_edge& e : edges)
		insert_edge_in_poly(bd, e);

	std::ofstream f(filename.c_str());

	f << g << " " << n << " " << false << true << std::endl << std::endl;
	
	add_generic_info_write_to_file(f);
	
	sew_boundary(v);
}


Dart_handle Input_Parser::find_edge_to_sew(const Dart_handle& ab_abd,
					   const unsigned long p,
					   const unsigned long q,
					   const unsigned long r)
{
	const Dart_handle bd_abd(cm.beta<1>(ab_abd)),
		da_abd(cm.beta<0>(ab_abd));

	const unsigned long a(cm.info<0>(ab_abd)),
		b(cm.info<0>(bd_abd)),
		d(cm.info<0>(da_abd));

	if(match_one(a, p, q, r) && match_one(b, p, q, r))
		return ab_abd;
	else if(match_one(b, p, q, r) && match_one(d, p, q, r))
		return bd_abd;
	else
		return da_abd;
}


void Input_Parser::add_last_vertex_info
(const Dart_handle& abc, const unsigned long a, const unsigned long b,
 const unsigned long c)
{
	if((cm.info<0>(abc) == a && cm.info<0>(cm.beta<1>(abc)) == b)
	   || (cm.info<0>(abc) == b && cm.info<0>(cm.beta<1>(abc)) == a))
		cm.set_attribute<0>(cm.beta<0>(abc), cm.create_attribute<0>(c));
	else if((cm.info<0>(abc) == b && cm.info<0>(cm.beta<1>(abc)) == c)
		|| (cm.info<0>(abc) == c && cm.info<0>(cm.beta<1>(abc)) == b))
		cm.set_attribute<0>(cm.beta<0>(abc), cm.create_attribute<0>(a));
	else
		cm.set_attribute<0>(cm.beta<0>(abc), cm.create_attribute<0>(b));
}

void Input_Parser::add_generic_info_write_to_file(std::ofstream& f)
{
	Face_iterator ab_abc(cm.one_dart_per_cell<2>().begin()),
		itend(cm.one_dart_per_cell<2>().end());

	for(; ab_abc != itend; ++ab_abc)
	{
		const Dart_handle bc_abc(cm.beta(ab_abc, 1)),
			ca_abc(cm.beta(ab_abc, 0));

		unsigned long a(cm.info<0>(ab_abc)),
			b(cm.info<0>(bc_abc)),
			c(cm.info<0>(ca_abc));
		
		cm.info(ab_abc).a = one;
		cm.info(bc_abc).a = one;
		cm.info(ca_abc).a = one;

		cm.set_attribute<2>(ab_abc, cm.create_attribute<2>(one));
	
		f << a << " " << b << " " << c << " "
		  << 1 << " " << 1 << " " << 1 << " " << 1 << std::endl;
	}
}


void Input_Parser::sew_boundary(const Boundary& bd)
{
	const unsigned long halfway(N / 2), s_fg(2 * g);
	unsigned long i;
	
	/* Identify the edges of the polygon. In the end, the word read
	   when moving on the sides of the polygon is:
	   - for S_1_1: abab
	   - for S_1_2: abcabc
	   - for S_1_3: abcdabdc
	   - for S_2_1: abcdabcd
	   - for S_2_5: abcdefghabcdhgfe
	   - for S_4_1: abcdefghabcdefgh
	*/
	
	// Sew the edges representing the fundamental group of S_g
	for(i = 0; i < s_fg; ++i)
		cm.sew<2>(bd.at(i), bd.at(i + halfway));

	// Sew the edges added due to the n punctures
	for(i = 0; i < n - 1; ++i)
		cm.sew<2>(bd.at(s_fg + i), bd.at(N - 1 - i));

}

void Input_Parser::sew_boundary(const Boundary& bd, ifstream& input)
{
	unsigned long i, j, k;

	for(k = 0; k < N / 2; ++k)
	{
		input >> i >> j;

		cm.sew<2>(bd[i], bd[j]);
	}
}

void Input_Parser::insert_edge_in_poly(Boundary& bd, const Comb_edge& e)
{
	const unsigned long a(e.a),
		b(e.b);

	const Dart_handle bd_a(bd[a]),
		bd_b(bd[b]);

	Dart_handle actual_a(null), actual_b(null);

	find_in_orbit(bd_a, b, actual_a, actual_b);
	
	if(actual_a == null)
		find_in_orbit(bd_b, a, actual_b, actual_a);

	if(actual_a == null)
	{
		std::cout << "Incorrect input. Edge " << a << "-" << b
			  << " cannot be drawn" << std::endl;

		return;
	}

	bool update_bd(is_dist_2(bd, a, b, N));
	
	Dart_handle ab(cm.insert_cell_1_in_cell_2(actual_a, actual_b));

	if(update_bd)
	{
		if(cm.beta(ab, 1, 1, 1) == ab)
			ab = cm.beta(ab, 2);

		bd[cm.info<0>(ab)] = ab;
	}
}


void Input_Parser::find_in_orbit(const Dart_handle bd_a, const unsigned long b,
				 Dart_handle& actual_a, Dart_handle& actual_b)
{
	B1_orbit_iterator it(cm.darts_of_orbit<1>(bd_a).begin()),
		itend(cm.darts_of_orbit<1>(bd_a).end());

	for(; it != itend; ++it)
	{
		if(cm.info<0>(it) == b)
		{
			actual_a = bd_a;
			actual_b = it;
			
			break;
		}
	}
}

void Input_Parser::sew_internal_edge(Edge_map& all_edges, Dart_handle edge,
				     const unsigned long a,
				     const unsigned long b)
{
	const Comb_edge e(a, b);

	Edge_map_iterator search(all_edges.find(e));

	if(search == all_edges.end())
	{
		all_edges.insert({e, edge});
		return;
	}

	cm.sew<2>(edge, search->second);
}



}
