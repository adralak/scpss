# SCPSS

A program to study *strictly convex projective structures on surfaces* using 
A-coordinates.

# Requirements

This program requires the following libraries installed:

- CGAL (version 3.9 or any later version) : <https://www.cgal.org/download.html>

- gtkmm 3.0 (or any later version) : <https://www.gtkmm.org/en/download.html>

- eigen 3.0 (or any later version) : <https://eigen.tuxfamily.org/index.php?title=Main_Page#Requirements> **NB:** the Eigen directory must linked to a standard
include directory (e.g. /usr/include or /usr/local/include).

- librsvg (version 2.40.20 or any later version) : <https://gitlab.gnome.org/GNOME/librsvg>

It also requires the software Geomview : <http://www.geomview.org/> and 
CMake (version 2.8.11 or any later version) : <https://cmake.org/>

# Installation

Run CMake in this directory, then compile everything using the generated
makefile.

# Running the program

The default executable name is scpss. For now, it **must** remain in this
directory.

# Files to feed the program

This program will ask you to feed it files representing a strictly convex
projective structure on a punctured surface.
These files look like this:

```text
// sewing_table is a boolean stating whether the sewing table is specified
// cubic_roots is a boolean stating whether the given parameters are the cubic 
// roots of the actual parameters
genus number_of_punctures sewing_table cubic_roots

// -2 * chi lines follow 
a b c q_ab q_bc q_ca q_t

// - chi lines follow if sewing_table is not 0
bd bd_s
```

bd is a vertex; it stands in for the edge [bd, bd + 1].
bd_s is the same. bd and bd_s will be glued together.

If the sewing table is not specified, the default is:
The first 2 * genus rows are: i _____ i + 1 - chi 

with i ranging from 0 to 2 * genus - 1.

The remaining rows are: 2 * genus + j _____ 2 - 2 * chi + 1 - j

with j ranging from 0 to number_of_punctures - 2.

a, b and c are integers from 0 to -2 * chi + 1; they represent the
vertices of the fundamental polygon of the surface
q_ab, q_bc, q_ca are the oriented edges parameters 
q_t is the triangle parameter

## On cubic_roots

Cubic roots are more natural when working with these structures: the monodromy
matrices use the cubic roots, for example.
However, flips cannot be done using exact rational numbers while keeping track
of exact cubic roots. As such, when a flip occurs, the parameters are all 
changed to be their cube and the boolean cubic_roots is set to false.

## File -> New

Instead of building your own files by hand, the GUI gives you a way to generate 
a file visually. This can be found in the File menu. The "New" item will open
a dialog prompting you for a genus and a number of punctures. Once these
have been submitted, another window will open with a polygon and a text field.
In the text field, you can type in the edges you would like to add to the 
polygon to triangulate it. For example:
```text
0 2
2 5
5 7
```
Means you are adding the edges 0-2, 2-5 and 5-7 to the polygon.
You can hit the "Preview" button to visualize the edges you have added.
Once the polygon has been triangulated, you can hit "Submit" to load the
surface into the program. By default, the coordinates are all 1. It
will also output a file called "generic_s_g_n" with the associated 
triangulation file. 
The other two buttons "Cone" and "Zigzag" are an easy way of building
a standard triangulation. They will both prompt you for a vertex. 
"Cone" will link the chosen vertex to all other vertices, save its neighbors.
"Zigzag" will draw edges in a zigzag starting at the chosen vertex.

**NB:** the generated triangulation assumes the given parameters are the 
cubic roots of the actual parameters

# Changing the triangulation on the fly

There are a few ways available from within the app to change the triangulation
without builing a new file

## Compute -> Canonical cell decomp

This will flip the current triangulation to a canonical cell decomposition
of the surface and output a triangulation file describing this cell 
decomposition.

## Triangulation -> Parameters

This allows you to change the A coordinates of the structure on the fly.
A dialog will prompt you, showing on the left the gluing table of the 
triangulation, and on the right, editable text containing the current 
coordinates. These coordinates follow the same convention as the triangulation
files: the last one is the triangle parameter. They are output in the same
way as the gluing table is; the ith line of the gluing table uses the 
coordinates displayed at the ith of the text buffer.

## Triangulation -> Flip

This allows you to flip an edge of the triangulation. It will show you the
gluing table on the left, for reference, and ask you to input an edge.
Edges should be input as t(i) or t i for the ith edge of triangle t. 
