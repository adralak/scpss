#include "Tools/tools.hh"
#include "Parser/parse_input.hh"
#include "Computations/computations.hh"
#include "Draw/draw_2.hh"
#include "App/app.hh"

int main(int argc, char* argv[])
{
	auto gtk_app(Gtk::Application::create(argc, argv, "scpss"));

	SCPSS::Application app;

	return gtk_app->run(*app.get_window_ptr());
}
