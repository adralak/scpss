#ifndef TOOLS_HH
#define TOOLS_HH

#include <CGAL/Combinatorial_map.h>
#include <CGAL/Cell_attribute.h>

#include <CGAL/Real_embeddable_traits.h>
#include <CGAL/Exact_rational.h>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Triangle_3.h>
#include <CGAL/Point_3.h>
#include <CGAL/Segment_3.h>

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Geometry>

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <unordered_map>

namespace SCPSS
{

typedef CGAL::Exact_rational Coord;
typedef Coord Expr;
typedef Coord Outitude;
typedef std::vector<Coord> A_coords;

template<class T> void sort(T& a, T& b, T& c)
{
	T temp;
	
	if(a < b)
	{
		if(b < c)
			return;

		temp = b;
		b = c;
		c = temp;
	}
	else
	{
		if(c < b)
		{
			temp = a;
			a = c;
			c = temp;
			return;
		}

		if(c < a)
		{
			temp = c;
			c = a;
			a = b;
			b = temp;
			return;
		}

		temp = a;
		b = a;
		a = temp;
	}
}


template<class T> bool lexico(const T& a1, const T& b1, const T& c1,
			      const T& a2, const T& b2, const T& c2)
{
	if(a1 < a2)
		return true;
	else if(a2 < a1)
		return false;

	if(b1 < b2)
		return true;
	else if(b2 < b1)
		return false;

	if(c1 < c2)
		return true;
	else if(c2 < c1)
		return false;

	return false;
}


typedef Eigen::Matrix<double, 3, 3> Matrix_33d;
typedef Eigen::Matrix<Expr, 1, 3> Line;
typedef Eigen::Matrix<Expr, 3, 1> Vector_3;
typedef Eigen::Matrix<Expr, 3, 3> Matrix_33;
typedef Eigen::FullPivLU<Matrix_33> Full_LU_3;
typedef Eigen::PartialPivLU<Matrix_33> Partial_LU_3;
typedef Eigen::Matrix<Expr, 4, 1> Vector_4;
typedef Eigen::Matrix<Expr, 4, 4> Matrix_44;
typedef Eigen::Matrix<double, 4, 1> Vector_4d;
typedef Eigen::Matrix<Expr, 2, 2> Matrix_22;
typedef Eigen::Matrix<Expr, 2, 1> Vector_2;

typedef CGAL::Simple_cartesian<double> K;
typedef CGAL::Triangle_3<K> Triangle_3;
typedef CGAL::Point_3<K> Point_3;
typedef std::vector<Point_3> Points_3;
typedef std::vector<Triangle_3> Triangles_3;
typedef std::vector<Triangles_3> Orbits_3;


struct Eigenvalues
{
	Expr lambda[3];

	Eigenvalues()
	{
	}

	Expr& operator[](unsigned i)
	{
		return lambda[i];
	}

	const Expr& operator[](unsigned i) const
	{
		return lambda[i];
	}

	Eigenvalues& operator=(const Eigenvalues& e)
	{
		for(int i(0); i < 3; ++i)
			lambda[i] = e[i];

		return (*this);
	}
};

typedef std::vector<Eigenvalues> Eigenvalues_map;


struct Coord_pair
{
	Coord a, x;
	Matrix_33 m;

	Coord_pair() : a(0), x(0)
	{
	}
	
	Coord_pair(const Coord& a) : a(a), x(0)
	{
	}

	Coord_pair(const Coord& a, const Coord& x) : a(a), x(x)
	{
	}
};

struct Coord_pair_with_hash
{
	Coord a, x;
	Matrix_33 m;
	unsigned long i;

	Coord_pair_with_hash() : a(0), x(0), i(0)
	{
	}
	
	Coord_pair_with_hash(const Coord& a) : a(a), x(0), i(0)
	{
	}

	Coord_pair_with_hash(const Coord& a, const Coord& x) : a(a), x(x), i(0)
	{
	}

	Coord_pair_with_hash(const Coord& a, const Coord& x, unsigned long i)
		: a(a), x(x), i(i)
	{
	}
};


struct Comb_edge
{
	unsigned a, b;

	Comb_edge()
	{
	}
	
	Comb_edge(unsigned a, unsigned b) : a(a), b(b)
	{
	}

	Comb_edge& operator=(const Comb_edge& e)
	{
		a = e.a;
		b = e.b;

		return (*this);
	}
};
	
typedef std::vector<Comb_edge> Comb_edges;


struct CoordsItems
{
	template<class CMap> struct Dart_wrapper
	{
		typedef CGAL::Cell_attribute<CMap, unsigned long> Vertex;
          	//typedef CGAL::Cell_attribute<CMap, Coords_pair> Edge_coords;
		typedef CGAL::Cell_attribute<CMap, Coord_pair_with_hash>
		Face_coords;
		typedef CGAL::cpp11::tuple<Vertex, void,
					   Face_coords>
		Attributes;
		typedef Coord_pair_with_hash Dart_info;
	};
};


class Tools
{
public:
	typedef CGAL::Combinatorial_map<2, CoordsItems> CMap_2;
	typedef CMap_2::Dart_handle Dart_handle;
	typedef CGAL::Real_embeddable_traits<Coord>::To_double
	To_double_coord;
	typedef CGAL::Real_embeddable_traits<Expr>::To_double
	To_double_expr;
	typedef CMap_2::Dart_range::iterator Dart_iterator;
	typedef CMap_2::One_dart_per_cell_range<2>::iterator Face_iterator;
	typedef CMap_2::One_dart_per_cell_range<1>::iterator Edge_iterator;
	typedef CMap_2::One_dart_per_cell_range<0>::iterator Vertex_iterator;
	typedef CMap_2::One_dart_per_incident_cell_range<1,0>::iterator
	Star_iterator;
	typedef CMap_2::Attribute_range<0>::type::iterator
	Vertex_attribute_iterator;
	typedef CMap_2::Attribute_range<2>::type::iterator
	Face_attribute_iterator;
	typedef std::vector<Dart_handle> Vertices;
	typedef std::vector<Dart_handle> Edges;
	typedef std::vector<Dart_handle> Boundary;
	typedef CMap_2::Dart_of_orbit_range<1>::iterator B1_orbit_iterator;
	typedef CMap_2::One_dart_per_cell_range<2> Face_range;

	void output_gluing_table(std::ostream& out,
				 Face_range& face_range,
				 bool verbose = true)
	{
		set_hashes();

		if(verbose)
		{
			out << "Format: one line per triangle, displaying"
			    << " its neighbors"  << std::endl 
			    << " in order a(b) means edge b of triangle a"
			    << std::endl << std::endl;
		}
		
		for(Face_iterator
			    it(face_range.begin()),
			    itend(face_range.end());
		    it != itend; ++it)
		{
			out << hash(it) << ": "
			    << hash(cm.beta(it, 2)) << "("
			    << hash_dart(cm.beta(it, 2)) << ")"
			    << ", " << hash(cm.beta(it, 1, 2))
			    << "(" << hash_dart(cm.beta(it, 1, 2)) << ")"
			    << ", " << hash(cm.beta(it, 0, 2)) << "(" 
			    << hash_dart(cm.beta(it, 0, 2)) << ")    "
			    << std::endl;
		}

	}

	Face_range output_gluing_table(std::ostream& out, bool verbose = false)
	{
		Face_range f_r(cm.one_dart_per_cell<2>());

		output_gluing_table(out, f_r, verbose);

		return f_r;
	}

	void output_a_coords(std::ostream& out, Face_range& f_r)
	{
		Face_iterator it(f_r.begin()), itend(f_r.end());

		for(; it != itend; ++it)
		{
			out << cm.info(it).a << " "
			    << cm.info(cm.beta(it, 1)).a << " "
			    << cm.info(cm.beta(it, 0)).a << " "
			    << cm.info<2>(it).a << std::endl;
		}
	}
	
	void show_specs(std::ostream& out)
	{
		Face_range face_range(cm.one_dart_per_cell<2>());
		
		output_gluing_table(out, face_range);
		
		out << std::endl
		    << "Format: two lines per triangle. The first one displays "
		    << std::endl << "the A coordinates, " 
		    << "with the last" << std::endl << "one being the triangle"
		    << " coordinate." << std::endl
		    << "The second only differs in that it" << std::endl
		    << "displays the X coordinates" << std::endl
		    << "The coordinates are displayed in the same" << std::endl
		    << "order the neighbors were"
		    << std::endl << std::endl;

	       
		for(Face_iterator
			    it(face_range.begin()),
			    itend(face_range.end());
		    it != itend; ++it)
		{
			out << hash(it) << ": "
			    << to_double(cm.info(it).a) << ", "
			    << to_double(cm.info(cm.beta(it, 1)).a) << ", "
			    << to_double(cm.info(cm.beta(it, 0)).a) << ", "
			    << to_double(cm.info<2>(it).a)
			    << std::endl
			    << hash(it) << ": "
			    << to_double(cm.info(it).x) << ", "
			    << to_double(cm.info(cm.beta(it, 1)).x) << ", "
			    << to_double(cm.info(cm.beta(it, 0)).x) << ", "
			    << to_double(cm.info<2>(it).x)
			    << std::endl;
		}

		out << std::endl;
		
		cm.display_characteristics(out);
		out << std::endl;
	       
		cm.display_darts(out);
		
		out << std::endl;
	}

	
protected:
	CMap_2 cm;
	const Dart_handle null = cm.null_dart_handle;
	const To_double_coord to_double = To_double_coord();
	const To_double_expr to_double_expr = To_double_expr();
	unsigned g, n;
	unsigned long N;
	const Coord zero = 0, one = 1;
	const Vector_3 zero_3 = Vector_3(0, 0, 0);
	bool cubic_roots;

	inline int match_one(const unsigned long a, const unsigned long p,
			     const unsigned long q, const unsigned long r)
	{
		return (a == p || a == q || a == r);
	}

	bool adjacent(Dart_handle t0, Dart_handle t1)
	{
		Dart_handle ab(t0),
			bc(cm.beta(ab, 1)),
			ca(cm.beta(ab, 0));

		return (adjacent_edge(ab, t1) || adjacent_edge(bc, t1),
			adjacent_edge(ca, t1));
	}

	bool adjacent_edge(Dart_handle edge, Dart_handle t)
	{
		Dart_handle t_curr(cm.beta(t, 2)),
			t_b1(cm.beta(t, 1, 2)),
			t_b0(cm.beta(t, 0, 2));

		return (edge == t_curr || edge == t_b1 || edge == t_b0);
	}

	unsigned long set_hashes()
	{
		Face_iterator it(cm.one_dart_per_cell<2>().begin()),
			itend(cm.one_dart_per_cell<2>().end());

		unsigned long i;
	
		for(i = 0; it != itend; ++i, ++it)
		{
			set_hash(it, i);
			set_hash_dart(it, 0);
			set_hash_dart(cm.beta(it, 1), 1);
			set_hash_dart(cm.beta(it, 0), 2);
		}

		return i;
	}

	unsigned long hash(const Dart_handle& d)
	{
		return cm.info<2>(d).i;
	}

	void set_hash(const Dart_handle& d, unsigned long i)
	{
		cm.info<2>(d).i = i;
	}

	void set_hash_dart(const Dart_handle& d, unsigned long i)
	{
		cm.info(d).i = i;
	}

	unsigned long hash_dart(const Dart_handle& d)
	{
		return cm.info(d).i;
	}

	bool is_dist_2(const Boundary& bd, const unsigned long a,
		       const unsigned long b, const unsigned long N)
	{
		return (cm.beta(bd[a], 1, 1) == bd[b]
			|| cm.beta(bd[a], 0, 0) == bd[b]);
	}

		
	void incr_mod(unsigned long& a, const unsigned long N)
	{
		a = a + 1 == N? 0 : a + 1;
	}

	void n_incr_mod(unsigned long& a, const unsigned long N,
			const unsigned long n)
	{
		a = a + n >= N? a + n - N : a + n;
	}

	void decr_mod(unsigned long& a, const unsigned long N)
	{
		a = a? a - 1 : N - 1;
	}

	Coord cube(const Coord& c)
	{
		return c * c * c;
	}
	
	Coord get_coord(const Coord& c)
	{
		return cubic_roots? cube(c) : c;
	}

	Expr get_cbrt(Dart_handle edge, Dart_handle sister)
	{
		const Coord &q_p(cm.info(edge).x),
			&q_m(cm.info(sister).x);

		return cubic_roots? (q_p / q_m) : Expr(std::cbrt
						       (to_double(q_p / q_m)));
	}

	Expr get_cbrt(Dart_handle t)
	{
		const Coord &t_p(cm.info<2>(t).x);

		return cubic_roots? t_p : Expr(std::cbrt(to_double(t_p)));
	}

	unsigned long get_number(Dart_handle dh)
	{
		unsigned long ht(hash(dh)), hd(hash_dart(dh));

		return 3 * ht + hd;
	}
};

}
#endif
