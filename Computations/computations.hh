#ifndef COMPUTATIONS_HH
#define COMPUTATIONS_HH

#include <cmath>
#include <queue>

#include "../Parser/parse_input.hh"
#include "../Decorated_triangles/decorated_triangle.hh"

#define PREC 0.00001

namespace SCPSS
{


class Computations : public Input_Parser
{
public:
	struct Cloverleaf_info
	{
		Decorated_triangle_x t;
		Dart_handle edge;
		unsigned where;

		Cloverleaf_info()
		{
		}

		Cloverleaf_info(const Decorated_triangle_x& t,
				const Dart_handle& edge,
				const unsigned where) :
			t(t), edge(edge), where(where)
		{
		}
	};

	struct Hypersurface_info
	{
		Decorated_triangle_a t;
		Dart_handle edge;
		unsigned where;
		
		Hypersurface_info()
		{
		}

		Hypersurface_info(const Decorated_triangle_a& t,
				  const Dart_handle& edge,
				  const unsigned where) :
			t(t), edge(edge), where(where)
		{
		}
	};

	struct Comb_triangle
	{
		unsigned long v[3];
		unsigned long new_id;

		Comb_triangle()
		{
			v[0] = 2;
			v[1] = 0;
			v[2] = 1;
			new_id = 3;
		}

		unsigned long& operator[](unsigned i)
		{
			return v[i % 3];
		}

		const unsigned long& operator[](unsigned i) const
		{
			return v[i % 3];
		}

		void adjacent(unsigned where)
		{
			const unsigned long a(v[where % 3]),
				b(v[(where + 1) % 3]);
			
			v[0] = b;
			v[1] = a;
			v[2] = new_id;
			++new_id;
		}

		Comb_triangle& operator=(const Comb_triangle& cm_t)
		{
			for(int i(0); i < 3; ++i)
				v[i] = cm_t[i];

			return (*this);
		}
	};

	struct Concrete_comb_triangle
	{
		Dart_handle dh;
		Comb_triangle t;
		unsigned where;

		Concrete_comb_triangle()
		{
		}
		
		Concrete_comb_triangle(Dart_handle dh, const Comb_triangle& t,
				       unsigned where)
			: dh(dh), t(t), where(where)
		{
		}

		Concrete_comb_triangle& operator=
		(const Concrete_comb_triangle& c_cm_t)
		{
			dh = c_cm_t.dh;
			t = c_cm_t.t;
			where = c_cm_t.where;

			return (*this);
		}
	};

	struct Hash_dart
	{
		std::size_t operator()(const unsigned long& h) const
		{
			std::size_t hash(std::hash<unsigned long>{}(h));

			return hash;
		}
	};

	typedef std::unordered_map<unsigned long, long, Hash_dart>
	Boundary_map;
	typedef Boundary_map::iterator Bd_map_iterator;

	
	typedef std::vector<Concrete_comb_triangle> Conc_cm_triangles;	
	typedef std::queue<Cloverleaf_info> Q_decorated_triangles;
	typedef std::queue<Hypersurface_info> Q_decorated_triangles_3;
	typedef CMap_2::size_type Mark;
	typedef std::vector<unsigned long> Permutation;
	typedef std::queue<Dart_handle> Q_dual_edges;
	
	Computations();
	
	Computations(ifstream& input);

	Computations(unsigned g, unsigned n, const Comb_edges& edges,
		     const std::string& filename);
	
	void canonical_cell_decomposition(std::ostream& f);

	void show_outitudes();

	void build_cloverleaf_position(Orbits_2& cloverleaf,
				       const unsigned depth = 15,
				       unsigned i = 0);

	void build_hypersurface(Orbits_3& hypersurface,
				const unsigned depth = 15,
				unsigned i = 0);
	
	void build_hypersurface(Orbits_s3& hypersurface,
				const unsigned depth = 15,
				unsigned i = 0);

	void get_all_eigenvalues(Eigenvalues_map& m);

	void output_monodromy_matrices(std::ostream& f);

	void set_a_coords(const A_coords& ac, Face_range& f_r);

	void flip(unsigned long trg, unsigned edge);

	void output_triangulation_file(std::ostream& f);
	
private:
	Matrix_33 alpha;

	const Expr e_zero = Expr(0);
	
	Dart_handle flip(Dart_handle& ca_abc);
	
	Dart_handle flip_edge(Dart_handle& edge);
	
	Outitude outitude(Dart_handle& ab_abc);

	void x_coords();

	void x_coords(Dart_handle& ca_abc);

	Dart_handle get_handle(unsigned i);

	void mark_edge(Dart_handle edge, Mark mk);

	void mark_triangle(Dart_handle edge, Mark mk);

	bool is_marked_triangle(const Dart_handle& t, Mark mk);
	
	void build_cloverleaf_position(Dart_handle& edge,
				       Orbits_2& cloverleaf,
				       const unsigned depth = 15);
		
	Line get_line(const Vector_3& A, const Vector_3& B);
 
	void get_lambda_mu(const Line& a, const Line& b, const Line& c,
			   Expr& lambda, Expr& mu);

	Vector_3 find_intersection(const Line& a, const Line& b);

	Decorated_triangle_x get_neighbor_x(const Decorated_triangle_x& t,
					Dart_handle edge,
					const unsigned where);

	void get_useful_x_coords(Dart_handle edge, Expr& e_p, Expr& e_m,
			       Expr& t_e_m);

	Triangle cloverleaf_triangle(const Decorated_triangle_x& t);

	Vector_3 standard_coords(const Vector_3& v);

	Decorated_triangle_a get_neighbor_a(const Decorated_triangle_a& t,
					  Dart_handle edge,
					  const unsigned where);

	Vector_3 get_vertex(const Vector_3& A, const Vector_3& C,
			    const Line& e_A, const Line& e_C,
			    const Expr& t, const Expr& e_A_B,
			    const Expr& e_C_B);

	Line get_line(const Vector_3& A, const Vector_3& C,
		      const Vector_3& B, const Expr& e_B_A,
		      const Expr& e_B_C);

	
	void get_useful_a_coords(Dart_handle ac_acd, Expr& t_abc,
				 Expr& e_A_B, Expr& e_C_B, Expr& e_B_A,
				 Expr& e_B_C);

	void build_hypersurface(Dart_handle& ca_abc, Orbits_3& hypersurface,
				const unsigned depth = 15);

	void build_hypersurface(Dart_handle& ca_abc,
				Orbits_s3& hypersurface,
				const unsigned depth = 5);

	bool is_valid(const Decorated_triangle_a& t, Dart_handle ca_abc);

	void monodromy_matrices();

	void monodromy_matrices_oriented_edges(const Dart_handle ab);

	void monodromy_matrix(const Dart_handle t);

	void monodromy_matrix(const Dart_handle& edge, const Dart_handle& sister,
			      Matrix_33& m);

	void get_eigenvalues(const Dart_handle& vertex, Eigenvalues& lambda);

	void output_triangle(std::ostream& f, Dart_handle t,
			     const Comb_triangle& c_t);

	void output_bd_gluing_table(std::ostream& f, const Boundary& bd);
	
	void unmark_triangle(Dart_handle t, Mark mk);

	void unmark_edge(Dart_handle edge, Mark mk);

	void mark_comb_edge(Dart_handle edge, Mark mk);

	void unmark_comb_edge(Dart_handle edge, Mark mk);
	
	void backtrack_poly_build(Conc_cm_triangles& trgs,
				  unsigned long& return_to,
				  unsigned long& i, Dart_handle& curr,
				  Comb_triangle& curr_t, Mark boundary,
				  Mark seen);

	void rearrange_vertices(Conc_cm_triangles& trgs, Boundary& bd,
				const unsigned long n_t);

	void build_boundary(const Conc_cm_triangles& trgs, Boundary& bd,
			    Mark boundary);

	void add_to_boundary(Dart_handle edge, Boundary& bd,
			     const unsigned where, Mark boundary);
	
	unsigned relative_pos(Dart_handle edge,
			      const Concrete_comb_triangle& t);

};

}


#endif
