#include "computations.hh"

#define EXPR(x) (x)(0)


namespace SCPSS
{

typedef Input_Parser::Dart_handle Dart_handle;		

Computations::Computations()
{
}


Computations::Computations(ifstream& input) : Input_Parser(input)
{
	x_coords();
	monodromy_matrices();
	
	const Expr root_3(std::sqrt(3));
	
	alpha << -1.5,      -1.5,         1,
		root_3 / 2, -root_3 / 2,  0,
		  0,         0,           1;
}

Computations::Computations(unsigned g, unsigned n, const Comb_edges& edges,
			   const std::string& filename) :
	Input_Parser(g, n, edges, filename)
{
	x_coords();
	monodromy_matrices();
	
	const Expr root_3(std::sqrt(3));
	
	alpha << -1.5,      -1.5,         1,
		root_3 / 2, -root_3 / 2,  0,
		  0,         0,           1;
}


void Computations::x_coords()
{
	for(Edge_iterator
		    it(cm.one_dart_per_cell<1>().begin()),
		    itend(cm.one_dart_per_cell<1>().end());
	    it != itend; ++it)
		x_coords(it);
}

void Computations::x_coords(Dart_handle& ca_abc)
{
        // Get the useful darts
	const Dart_handle ab_abc(cm.beta(ca_abc, 1)),
		bc_abc(cm.beta(ca_abc, 0)),
		ac_acd(cm.beta(ca_abc, 2)),
		cd_acd(cm.beta(ac_acd, 1)),
		da_acd(cm.beta(ac_acd, 0));

	// Get the surrounding coords
	const Coord &e_m(cm.info(ca_abc).a),
		&e_p(cm.info(ac_acd).a),
		&a_m(cm.info(ab_abc).a),
		&a_p(cm.info(cm.beta(ab_abc, 2)).a),
		&b_m(cm.info(bc_abc).a),
		&b_p(cm.info(cm.beta(bc_abc, 2)).a),
		&d_p(cm.info(da_acd).a),
		&d_m(cm.info(cm.beta(da_acd, 2)).a),
		&c_p(cm.info(cd_acd).a),
		&c_m(cm.info(cm.beta(cd_acd, 2)).a),
		&A(cm.info<2>(ca_abc).a),
		&B(cm.info<2>(ac_acd).a);

	// Compute edge coords
	const Coord interm_qp1(A * d_m),
		interm_qm1(B * b_p),
		interm_qp2(B * a_m),
		interm_qm2(A * c_p),
		qp(interm_qp1 / interm_qp2),
		qm(interm_qm1 / interm_qm2);

	cm.info(ca_abc).x = qm;
	cm.info(ac_acd).x = qp;

	// Compute triangle coords
	const Coord interm_tA1(a_m * b_m * e_m),
		interm_tA2(a_p * b_p * e_p),
		interm_tB1(c_p * d_p * e_p),
		interm_tB2(c_m * d_m * e_m),
		tA(interm_tA1 / interm_tA2),
		tB(interm_tB1 / interm_tB2);
	
	cm.info<2>(ca_abc).x = tA;
	cm.info<2>(ac_acd).x = tB;
}


void Computations::canonical_cell_decomposition(std::ostream& f)
{
	bool flipped;

	Outitude o;

	do
	{
		for(Edge_iterator
			    it(cm.one_dart_per_cell<1>().begin()),
			    itend(cm.one_dart_per_cell<1>().end());
		    it != itend; ++it)
		{
			o = outitude(it);

			flipped = o < zero;
			if(flipped)
			{
				flip(it);
				break;
			}
		}
	} while(flipped);

	x_coords();
	monodromy_matrices();
	output_triangulation_file(f);
}

void Computations::flip(unsigned long trg, unsigned edge)
{
	if(trg > cm.one_dart_per_cell<2>().size())
	{
		std::cout << "Triangle number " << trg << " does not exist"
			  << std::endl;
		return;
	}

	if(edge > 2)
	{
		std::cout << "Triangles only have 3 edges numbered 0, 1 and 2"
			  << std::endl;
		return;
	}
	
	Face_iterator f_it(cm.one_dart_per_cell<2>().begin()),
		f_itend(cm.one_dart_per_cell<2>().end());

	for(; f_it != f_itend; ++f_it)
	{
		if(hash(f_it) == trg)
			break;
	}

	B1_orbit_iterator b1_it(cm.darts_of_orbit<1>(f_it).begin()),
		b1_itend(cm.darts_of_orbit<1>(f_it).end());

	for(; b1_it != b1_itend; ++b1_it)
	{
		if(hash_dart(b1_it) == edge)
			break;
	}

	Dart_handle new_edge(flip(b1_it)),
		sister(cm.beta(new_edge, 2));

	Matrix_33 &new_m(cm.info(new_edge).m),
		&new_m_sister(cm.info(sister).m);
	
	x_coords(new_edge);
	monodromy_matrix(new_edge, sister, new_m);
	monodromy_matrix(sister, new_edge, new_m_sister);
	monodromy_matrix(new_edge);
	monodromy_matrix(sister);
}


Dart_handle Computations::flip(Dart_handle& ca_abc)
{
	if(cubic_roots)
	{
		cubic_roots = false;

		Face_iterator f_it(cm.one_dart_per_cell<2>().begin()),
			f_itend(cm.one_dart_per_cell<2>().end());

		for(; f_it != f_itend; ++f_it)
		{
			cm.info<2>(f_it).a = cube(cm.info<2>(f_it).a);

			B1_orbit_iterator
				b1_it(cm.darts_of_orbit<1>(f_it).begin()),
				b1_itend(cm.darts_of_orbit<1>(f_it).end());

			for(; b1_it != b1_itend; ++b1_it)
				cm.info(b1_it).a = cube(cm.info(b1_it).a);
		}
	}
	
        // Get the useful darts
	const Dart_handle ab_abc(cm.beta(ca_abc, 1)),
		bc_abc(cm.beta(ca_abc, 0)),
		ac_acd(cm.beta(ca_abc, 2)),
		cd_acd(cm.beta(ac_acd, 1)),
		da_acd(cm.beta(ac_acd, 0));

	// Get the surrounding coords
	const Coord &e_m(cm.info(ca_abc).a),
		&e_p(cm.info(ac_acd).a),
		&a_m(cm.info(ab_abc).a),
		&a_p(cm.info(cm.beta(ab_abc, 2)).a),
		&b_m(cm.info(bc_abc).a),
		&b_p(cm.info(cm.beta(bc_abc, 2)).a),
		&d_p(cm.info(da_acd).a),
		&d_m(cm.info(cm.beta(da_acd, 2)).a),
		&c_p(cm.info(cd_acd).a),
		&c_m(cm.info(cm.beta(cd_acd, 2)).a),
		&A(cm.info<2>(ca_abc).a),
		&B(cm.info<2>(ac_acd).a);

	// Compute new coords
	const Coord interm_C(A * c_p + B * b_p),
		interm_D(A * d_m + B * a_m),
		C(interm_C / e_m),
		D(interm_D / e_p),
		interm_f_p(C * a_p + D * b_m),
		interm_f_m(C * d_p + D * c_m),
		f_p(interm_f_p / A),
		f_m(interm_f_m / B);

	// Actually flip the edge
	const Dart_handle db_dbc(flip_edge(ca_abc)),
		bd_abd(cm.beta(db_dbc, 2));
	
	cm.set_attribute<2>(bd_abd, cm.create_attribute<2>(D));
	cm.set_attribute<2>(db_dbc, cm.create_attribute<2>(C));
	cm.info(bd_abd).a = f_p;
	cm.info(db_dbc).a = f_m;

	return db_dbc;
}


Outitude Computations::outitude(Dart_handle& ca_abc)
{
	// Get the useful darts
	const Dart_handle ab_abc(cm.beta(ca_abc, 1)),
		cb_cbf(cm.beta(ca_abc, 0, 2)),
		ac_acd(cm.beta(ca_abc, 2)),
		cd_acd(cm.beta(ac_acd, 1)),
		ad_adh(cm.beta(ac_acd, 0, 2));

	// Get the useful coords
	const Coord &e_m(get_coord(cm.info(ca_abc).a)),
		&e_p(get_coord(cm.info(ac_acd).a)),
		&a_m(get_coord(cm.info(ab_abc).a)),
		&b_p(get_coord(cm.info(cb_cbf).a)),
		&d_m(get_coord(cm.info(ad_adh).a)),
		&c_p(get_coord(cm.info(cd_acd).a)),
		&A(get_coord(cm.info<2>(ca_abc).a)),
		&B(get_coord(cm.info<2>(ac_acd).a));

	// Compute outitude
	const Outitude e_p_e_m(e_p * e_m),
		o(A * (e_m * d_m + e_p * c_p - e_p_e_m) +
		  B * (e_m * a_m + e_p * b_p - e_p_e_m));

	return o;
}


Dart_handle Computations::flip_edge(Dart_handle& edge)
{
	Dart_handle p(cm.beta(edge, 0)),
		q(cm.beta(edge, 2, 0)),
		pq;

	cm.remove_cell<1>(edge);
	pq = cm.insert_cell_1_in_cell_2(p, q);

	return pq;
}


void Computations::show_outitudes()
{
	for(Edge_iterator
		    it(cm.one_dart_per_cell<1>().begin()),
		    itend(cm.one_dart_per_cell<1>().end());
	    it != itend; ++it)
		std::cout << to_double(outitude(it)) << "; ";

	std::cout << std::endl;
}

Dart_handle Computations::get_handle(unsigned i)
{
	Face_iterator it(cm.one_dart_per_cell<2>().begin()),
		    itend(cm.one_dart_per_cell<2>().end());

	if(i >= cm.one_dart_per_cell<2>().size())
		return it;
	
	for(; it != itend && i > 0; --i, ++it);

	return it;
}

void Computations::mark_edge(Dart_handle edge, Mark mk)
{
	cm.mark(edge, mk);
}

void Computations::mark_triangle(Dart_handle edge, Mark mk)
{
	mark_edge(edge, mk);
	mark_edge(cm.beta(edge, 1), mk);
	mark_edge(cm.beta(edge, 0), mk);
}

bool Computations::is_marked_triangle(const Dart_handle& t, Mark mk)
{
	const bool a(cm.is_marked(t, mk)),
		b(cm.is_marked(cm.beta(t, 1), mk)),
		c(cm.is_marked(cm.beta(t, 0), mk));

	return (a && b && c);
}


void Computations::build_cloverleaf_position(Orbits_2& cloverleaf,
					     const unsigned depth,
					     unsigned i)
{
	Dart_handle base_trg(get_handle(i));
	
	unsigned long n(set_hashes());

	cloverleaf.resize(n);
	build_cloverleaf_position(base_trg, cloverleaf, depth);
}

void Computations::build_hypersurface(Orbits_3& hypersurface,
				     const unsigned depth,
				     unsigned i)
{
	Dart_handle base_trg(get_handle(i));

	unsigned long n(set_hashes());

	hypersurface.resize(n);
	build_hypersurface(base_trg, hypersurface, depth);
}



void Computations::build_hypersurface(Orbits_s3& hypersurface,
				      const unsigned depth,
				      unsigned i)
{
	Dart_handle base_trg(get_handle(i));

	unsigned long n(set_hashes());

	hypersurface.resize(n);
	build_hypersurface(base_trg, hypersurface, depth);
}

void Computations::output_monodromy_matrices(std::ostream& f)
{
	Face_range f_r(cm.one_dart_per_cell<2>());

	output_gluing_table(f, f_r, false);

	f << std::endl;
	
	Face_iterator it(f_r.begin()),
		itend(f_r.end());

	long i(1);

	Dart_handle curr(f_r.begin()),
		bd_edge(null);

	Mark seen(cm.get_new_mark()),
		boundary(cm.get_new_mark());

	Boundary_map bd_map;

	Matrix_33 final_m;
	
	Q_dual_edges q;

	mark_triangle(curr, seen);
	q.push(curr);
	q.push(cm.beta(curr, 1));
	q.push(cm.beta(curr, 0));

	while(!q.empty())
	{
		curr = q.front();
		q.pop();

		const Dart_handle sister(cm.beta(curr, 2));
				
		if(cm.is_marked(sister, seen))
		{
			mark_comb_edge(curr, boundary);
			bd_map.insert({get_number(curr), i});
			bd_map.insert({get_number(sister), -i});

			if(bd_edge == null)
				bd_edge = curr;

			++i;
			
			continue;
		}

		mark_triangle(sister, seen);
		q.push(cm.beta(sister, 1));
		q.push(cm.beta(sister, 0));
	}

	cm.negate_mark(seen);

	Bd_map_iterator bd_it(bd_map.begin()),
		bd_itend(bd_map.end());

	for(; bd_it != bd_itend; ++bd_it)
		f << bd_it->first << " = " << bd_it->second << "; ";

	f << std::endl << bd_map[get_number(bd_edge)];

	final_m = cm.info(bd_edge).m;
	
	while(!cm.is_marked(bd_edge, seen))
	{
		mark_edge(bd_edge, seen);
		bd_edge = cm.beta(bd_edge, 1);
		
		while(!cm.is_marked(bd_edge, seen))
		{
			if(cm.is_marked(bd_edge, boundary))
				break;
			
			mark_edge(bd_edge, seen);
			bd_edge = cm.beta(bd_edge, 2, 1);
		}

		if(cm.is_marked(bd_edge, seen))
			break;
		
		f << " * " << bd_map[get_number(bd_edge)];
		final_m *= cm.info(bd_edge).m;
	}

	f << "final matrix: " << std::endl << std::endl << final_m
	  << std::endl << std::endl;

	cm.negate_mark(seen);
	cm.negate_mark(boundary);
	cm.free_mark(seen);
	cm.free_mark(boundary);
	
	for(; it != itend; ++it)
	{
		f << hash(it) << ":" << std::endl;
		
		for(i = 0, curr = it; i < 3; ++i, curr = cm.beta(curr, 1))
		{
			f << hash_dart(curr) << ":" << std::endl
			  << cm.info(curr).m << std::endl << std::endl;
		}

		f << "triangle: " << std::endl
		  << cm.info<2>(curr).m << std::endl << std::endl;
	}
}

void Computations::set_a_coords(const A_coords& ac, Face_range& f_r)
{
	Face_iterator it(f_r.begin()), itend(f_r.end());

	unsigned long i(0);

	for(; it != itend; ++it, i += 4)
	{
		const Dart_handle next(cm.beta(it, 1)),
			prev(cm.beta(it, 0));

		cm.info(it).a = ac[i];
		cm.info(next).a = ac[i + 1];
		cm.info(prev).a = ac[i + 2];
		cm.info<2>(it).a = ac[i + 3];
	}

	x_coords();
	monodromy_matrices();
}


/* Builds the cloverleaf position from the triangle and edge represented
   by edge */
void Computations::build_cloverleaf_position(Dart_handle& edge,
					     Orbits_2& cloverleaf,
					     const unsigned depth)
{
	// Queue info
	Q_decorated_triangles q, next_q;

	// Triangles that have already been drawn
	Ordered_triangles already_drawn;

	// Build the initial triangle and its neighbors
	const Vector_3 unit_x(1, 0, 0),
		unit_y(0, 1, 0),
		unit_z(0, 0, 1);

	const Coord &t(cm.info<2>(edge).x);
	/*
	const double dt(to_double(t)),
		cbr_t(std::cbrt(dt));
	*/
	const Line e_x(0, t, 1),
		e_y(1, 0, t),
		e_z(t, 1, 0);
	
	const Decoration D0(unit_x, e_x),
		D1(unit_y, e_y),
		D2(unit_z, e_z);

	const Decorated_triangle_x initial_t(D0, D1, D2);
	
	const Cloverleaf_info i0(initial_t, edge, THIS),
		i1(initial_t, cm.beta(edge, 1), NEXT),
		i2(initial_t, cm.beta(edge, 0), PREV);

	Mark mk(cm.get_new_mark());
	
	q.push(i0);
	q.push(i1);
	q.push(i2);

	cloverleaf[hash(edge)].push_back(cloverleaf_triangle(initial_t));

	mark_triangle(edge, mk);

	for(unsigned i(0); i < depth; ++i)
	{
		while(!q.empty())
		{
			const Cloverleaf_info& curr_t(q.front());

			const Decorated_triangle_x& t(curr_t.t);

			const Dart_handle& edge(curr_t.edge);

			const unsigned where(curr_t.where);

			if(is_marked_triangle(cm.beta(edge, 2), mk))
			{
				next_q.push(curr_t);

				q.pop();
				continue;				
			}

			const Decorated_triangle_x
				neighbor(get_neighbor_x(t, edge, where));

			const Ordered_triangle
				neighbor_ordered(neighbor.ordered_triangle());

			if(already_drawn.find(neighbor_ordered)
			   != already_drawn.end())
			{
				q.pop();
				continue;
			}

			already_drawn.insert(neighbor_ordered);
			
			const Dart_handle sister(cm.beta(edge, 2)),
				nd1(cm.beta(sister, 1)),
				nd2(cm.beta(sister, 0));

			const Cloverleaf_info ni1(neighbor, nd1, NEXT),
				ni2(neighbor, nd2, PREV);

			q.push(ni1);
			q.push(ni2);

			cloverleaf[hash(sister)].push_back
				(cloverleaf_triangle(neighbor));

			mark_triangle(sister, mk);

			q.pop();
		}

		cm.negate_mark(mk);

		q = next_q;
		next_q = Q_decorated_triangles();
	}

	cm.free_mark(mk);
}

// Get the line through A and B
Line Computations::get_line(const Vector_3& A, const Vector_3& B)
{
	Matrix_33 m;

	const Line At(A.transpose()),
		Bt(B.transpose());
	
	m << At, Bt, At + Bt;

	Full_LU_3 lu_m(m);

	const Vector_3& k(lu_m.kernel().col(0));

	const Line line(k.transpose());

	return line;
}


// Given a, b and c, find lambda and mu such that c = lambda * a + mu * b
void Computations::get_lambda_mu(const Line& a, const Line& b, const Line& c,
				 Expr& lambda, Expr& mu)
{
	Matrix_33 m;

	m << a.transpose(), b.transpose(), zero_3;

	Vector_3 lambda_mu(m.fullPivLu().solve(c.transpose()));
	
	lambda = lambda_mu(0);
	mu = lambda_mu(1);
}


// Find the intersection of two lines
Vector_3 Computations::find_intersection(const Line& a, const Line& b)
{
	Matrix_33 m;

	m << a, b, zero_3.transpose();

	Full_LU_3 lu_m(m);

	const Vector_3 intersection(lu_m.kernel().col(0));

	return intersection;
}

// Computes the neighbor of t with respect to the edge given
Decorated_triangle_x Computations::get_neighbor_x(const Decorated_triangle_x& t,
					      Dart_handle edge,
					      const unsigned where)
{
	// These will store the useful coordinates
	Expr e_02, e_20, t_012,
		// These will hold numbers used to find the missing vertex
		lambda, mu;

	// Get the decorations
	const Decoration &D0(t[where]),
		&D2(t[where + 1]),
		&D3(t[where + 2]);

	// Exctract the vertices
	const Vector_3 &V0(D0.c),
		&V2(D2.c),
		&V3(D3.c);

	// Extract the edges
	const Line &V0V2(t(where)),
		&V2V3(t(where + 1)),
		&V3V0(t(where + 2)),
		// And the lines in the decoration
		&e0(D0.e),
		&e2(D2.e);
	
	// Get the useful coordinates
	get_useful_x_coords(edge, e_02, e_20, t_012);
	
	// Find the line V0V1 using the fact that e_02 is a crossratio
	get_lambda_mu(e0, V0V2, V3V0, lambda, mu);

	const Expr ratio_lambda_p_mu_p(- lambda * e_02 / mu);
	const Line V0V1(ratio_lambda_p_mu_p * e0 + V0V2);
	
	// Same thing for V2V1
	get_lambda_mu(e2, V0V2, V2V3, lambda, mu);
	
	const Expr ratio_mu_lambda(- mu * e_20 / lambda);
	const Line V2V1(e2 + ratio_mu_lambda * V0V2);
	
	// With V0V1 and V2V1, V1 can be found
	const Vector_3 V1(find_intersection(V0V1, V2V1));
	
	/* Compute the ratio e_1(V2) / e_1(V0) knowing that t_012 is 
	   a triple ratio */
	const Expr e0_V1(EXPR(e0 * V1)),
		e0_V2(EXPR(e0 * V2)),
		ratio_e0(e0_V2 / e0_V1),
		e2_V0(EXPR(e2 * V0)),
		e2_V1(EXPR(e2 * V1)),
		ratio_e2(e2_V1 / e2_V0),
		ratio_e1(t_012 * ratio_e0 * ratio_e2);

	// Compute e1 with this information
	const Line e1(get_line(V1, V2 - ratio_e1 * V0));

	const Expr e0_V3(EXPR(e0 * V3)),
		ratio_e0_1(e0_V3 / e0_V1),
		e2_V3(EXPR(e2 * V3)),
		ratio_e2_1(e2_V1 / e2_V3),
		V2V3_V1(EXPR(V2V3 * V1)),
		V2V3_V0(EXPR(V2V3 * V0)),
		ratio_V2V3(V2V3_V1 / V2V3_V0),
		V3V0_V2(EXPR(V3V0 * V2)),
		V3V0_V1(EXPR(V3V0 * V1)),
		ratio_V3V0(V3V0_V2 / V3V0_V1);
	
	const Decoration D1(V1, e1);

	// The new decorated triangle has thus been found
	Decorated_triangle_x new_t(t, D1, V0V1, V2V1, where);

	return new_t;
}

// Get the edge coordinates and the opposite triangle coordinate
void Computations::get_useful_x_coords(Dart_handle edge, Expr& e_p, Expr& e_m,
				       Expr& t_e_m)
{
	Dart_handle sister_edge(cm.beta(edge, 2));
	
	e_p = get_coord(cm.info(edge).x);
	e_m = get_coord(cm.info(sister_edge).x);
	t_e_m = get_coord(cm.info<2>(sister_edge).x);
}

// Compute the cloverleaf triangle from its decorated counterpart
Triangle Computations::cloverleaf_triangle(const Decorated_triangle_x& t)
{
	const Vector_3 &C0(t[0].c),
		&C1(t[1].c),
		&C2(t[2].c);
	
	const Vector_3 new_C0(alpha * standard_coords(C0)),
		new_C1(alpha * standard_coords(C1)),
		new_C2(alpha * standard_coords(C2));

	const Point_2 c0(to_double(new_C0(0)), to_double(new_C0(1))),
		c1(to_double(new_C1(0)), to_double(new_C1(1))),
		c2(to_double(new_C2(0)), to_double(new_C2(1)));
	
	Triangle cloverleaf_t(c0, c1, c2);

	return cloverleaf_t;
}

// Compute the standard coordinates of a vector
Vector_3 Computations::standard_coords(const Vector_3& v)
{
	const Expr z(v(0) + v(1) + v(2));

	if(z == e_zero)
	{
		std::cout << "Error while computing standard coords"
			  << std::endl;
		return Vector_3(0, 0, 0);
	}

	const Expr y(v(1) / z),
		x(v(0) / z);

	return Vector_3(x, y, 1);
}

bool Computations::is_valid(const Decorated_triangle_a& t, Dart_handle ca_abc)
{
	const Decoration &D0(t[0]),
		&D1(t[1]),
		&D2(t[2]);

	Dart_handle ab_abc(cm.beta(ca_abc, 1)),
		bc_abc(cm.beta(ca_abc, 0)),
		ba(cm.beta(ab_abc, 2)),
		cb(cm.beta(bc_abc, 2)),
		ac(cm.beta(ca_abc, 2));
	
	const Expr &e_A_B(get_coord(cm.info(ab_abc).a)),
		&e_A_C(get_coord(cm.info(ac).a)),
		&e_B_A(get_coord(cm.info(ba).a)),
		&e_B_C(get_coord(cm.info(bc_abc).a)),
		&e_C_A(get_coord(cm.info(ca_abc).a)),
		&e_C_B(get_coord(cm.info(cb).a)),
		&t_abc(get_coord(cm.info<2>(ca_abc).a));

	const Expr e12(EXPR(D1.e * D2.c)),
		e10(EXPR(D1.e * D0.c)),
		e21(EXPR(D2.e * D1.c)),
		e20(EXPR(D2.e * D0.c)),
		e01(EXPR(D0.e * D1.c)),
		e02(EXPR(D0.e * D2.c));
	
	const bool a(e_A_B == e12 && e_A_C == e10),
		b(e_B_A == e21 && e_B_C == e20),
		c(e_C_A == e01 && e_C_B == e02);

	Matrix_33 m;

	m << D0.c, D1.c, D2.c;

	const bool trg(m.determinant() == t_abc),
		valid(a && b && c && trg);
	
	return valid;	
}

Decorated_triangle_a Computations::get_neighbor_a(const Decorated_triangle_a& t,
						  Dart_handle edge,
						  const unsigned where)
{
	const Decoration &DA(t[where]),
		&DC(t[where + 1]);

	const Vector_3 &A(DA.c),
		&C(DC.c);

	const Line &e_A(DA.e),
		&e_C(DC.e);

	Expr t_abc, e_A_B, e_C_B, e_B_A, e_B_C;

	get_useful_a_coords(edge, t_abc, e_A_B, e_C_B, e_B_A, e_B_C);

	const Vector_3 B(get_vertex(A, C, e_A, e_C, t_abc, e_A_B, e_C_B));

	const Line e_B(get_line(A, C, B, e_B_A, e_B_C));

	const Decoration DB(B, e_B);

	const Decorated_triangle_a new_t(t, DB, where);

	return new_t;
}


Vector_3 Computations::get_vertex(const Vector_3& A, const Vector_3& C,
				 const Line& e_A, const Line& e_C,
				 const Expr& t, const Expr& e_A_B,
				 const Expr& e_C_B)
{
	const Vector_3 CxA(C.cross(A));

	Matrix_33 m;

	m << e_A, e_C, CxA.transpose();

	const Vector_3 constraints(e_A_B, e_C_B, t);

	const Vector_3 B(m.partialPivLu().solve(constraints));

	return B;
}


Line Computations::get_line(const Vector_3& A, const Vector_3& C,
			    const Vector_3& B, const Expr& e_B_A,
			    const Expr& e_B_C)
{
	Matrix_33 m;

	m << A.transpose(), C.transpose(), B.transpose();

	const Vector_3 constraints(e_B_A, e_B_C, 0);

	const Vector_3 line_t(m.partialPivLu().solve(constraints));

	const Line e_B(line_t.transpose());

	return e_B;
}


void Computations::get_useful_a_coords(Dart_handle ac_acd, Expr& t_abc,
				       Expr& e_A_B, Expr& e_C_B, Expr& e_B_A,
				       Expr& e_B_C)
{
	const Dart_handle ca_abc(cm.beta(ac_acd, 2)),
		ab_abc(cm.beta(ca_abc, 1)),
		bc_abc(cm.beta(ca_abc, 0)),
		ba_aeb(cm.beta(ab_abc, 2)),
		cb_cbf(cm.beta(bc_abc, 2));

	t_abc = get_coord(cm.info<2>(ca_abc).a);
	e_A_B = get_coord(cm.info(ab_abc).a);
	e_C_B = get_coord(cm.info(cb_cbf).a);
	e_B_A = get_coord(cm.info(ba_aeb).a);
	e_B_C = get_coord(cm.info(bc_abc).a);
}


void Computations::build_hypersurface(Dart_handle& ca_abc,
				      Orbits_3& hypersurface,
				      const unsigned depth)
{
        Q_decorated_triangles_3 q, next_q;

	Ordered_triangles already_drawn;
	
	const Dart_handle ac_acd(cm.beta(ca_abc, 2)),
		ab_abc(cm.beta(ca_abc, 1)),
		ba_aeb(cm.beta(ab_abc, 2)),
		bc_abc(cm.beta(ca_abc, 0)),
		cb_cbf(cm.beta(bc_abc, 2));

	const Expr &e_A_B(get_coord(cm.info(ab_abc).a)),
		&e_A_C(get_coord(cm.info(ac_acd).a)),
		&e_B_A(get_coord(cm.info(ba_aeb).a)),
		&e_B_C(get_coord(cm.info(bc_abc).a)),
		&e_C_A(get_coord(cm.info(ca_abc).a)),
		&e_C_B(get_coord(cm.info(cb_cbf).a)),
		&t_abc(get_coord(cm.info<2>(ca_abc).a));
	
	const Vector_3 initial_A(1, 0, 0),
		initial_B(0, t_abc, 0),
		initial_C(0, 0, 1);

	const Line e_A(get_line(initial_B, initial_C, initial_A, e_A_B, e_A_C)),
		e_B(get_line(initial_A, initial_C, initial_B, e_B_A, e_B_C)),
		e_C(get_line(initial_A, initial_B, initial_C, e_C_A, e_C_B));

	const Decoration DA(initial_A, e_A),
		DB(initial_B, e_B),
		DC(initial_C, e_C);

	const Decorated_triangle_a initial_t(DC, DA, DB);
	
	const Hypersurface_info i0(initial_t, ca_abc, THIS),
		i1(initial_t, ab_abc, NEXT),
		i2(initial_t, bc_abc, PREV);

	Mark mk(cm.get_new_mark());
	
	q.push(i0);
	q.push(i1);
	q.push(i2);

	hypersurface[hash(ca_abc)].push_back(initial_t.to_triangle3());

	mark_triangle(ab_abc, mk);

	for(unsigned i(0); i < depth; ++i)
	{
		while(!q.empty())
		{
			const Hypersurface_info& curr_t(q.front());

			const Decorated_triangle_a& t(curr_t.t);

			const Dart_handle& edge(curr_t.edge);

			const unsigned where(curr_t.where);

			if(is_marked_triangle(cm.beta(edge, 2), mk))
			{
				next_q.push(curr_t);

				q.pop();
				continue;				
			}

			const Decorated_triangle_a
				neighbor(get_neighbor_a(t, edge, where));

			const Ordered_triangle
				neighbor_ordered(neighbor.ordered_triangle());
			
			if(already_drawn.find(neighbor_ordered)
			   != already_drawn.end())
			{
				q.pop();
				continue;
			}

			already_drawn.insert(neighbor_ordered);
			
			const Dart_handle sister(cm.beta(edge, 2)),
				nd1(cm.beta(sister, 1)),
				nd2(cm.beta(sister, 0));

			const Hypersurface_info ni1(neighbor, nd1, NEXT),
				ni2(neighbor, nd2, PREV);

			q.push(ni1);
			q.push(ni2);

			hypersurface[hash(sister)].push_back
				(neighbor.to_triangle3());

			mark_triangle(sister, mk);

			q.pop();
		}

		cm.negate_mark(mk);

		q = next_q;
		next_q = Q_decorated_triangles_3();
	}

	cm.free_mark(mk);
}

void Computations::build_hypersurface(Dart_handle& ca_abc,
				      Orbits_s3& hypersurface,
				      const unsigned depth)
{
        Q_decorated_triangles_3 q, next_q;

	Ordered_triangles already_drawn;

	const Dart_handle ac_acd(cm.beta(ca_abc, 2)),
		ab_abc(cm.beta(ca_abc, 1)),
		ba_aeb(cm.beta(ab_abc, 2)),
		bc_abc(cm.beta(ca_abc, 0)),
		cb_cbf(cm.beta(bc_abc, 2));

	const Expr &e_A_B(get_coord(cm.info(ab_abc).a)),
		&e_A_C(get_coord(cm.info(ac_acd).a)),
		&e_B_A(get_coord(cm.info(ba_aeb).a)),
		&e_B_C(get_coord(cm.info(bc_abc).a)),
		&e_C_A(get_coord(cm.info(ca_abc).a)),
		&e_C_B(get_coord(cm.info(cb_cbf).a)),
		&t_abc(get_coord(cm.info<2>(ca_abc).a));
	
	const Vector_3 initial_A(1, 0, 0),
		initial_B(0, t_abc, 0),
		initial_C(0, 0, 1);

	const Line e_A(get_line(initial_B, initial_C, initial_A, e_A_B, e_A_C)),
		e_B(get_line(initial_A, initial_C, initial_B, e_B_A, e_B_C)),
		e_C(get_line(initial_A, initial_B, initial_C, e_C_A, e_C_B));

	const Decoration DA(initial_A, e_A),
		DB(initial_B, e_B),
		DC(initial_C, e_C);

	const Decorated_triangle_a initial_t(DC, DA, DB);
	
	const Hypersurface_info i0(initial_t, ca_abc, THIS),
		i1(initial_t, ab_abc, NEXT),
		i2(initial_t, bc_abc, PREV);

	Mark mk(cm.get_new_mark());
	
	q.push(i0);
	q.push(i1);
	q.push(i2);

	hypersurface[hash(ca_abc)].push_back(initial_t.to_triangle_s3());

	mark_triangle(ab_abc, mk);

	for(unsigned i(0); i < depth; ++i)
	{
		while(!q.empty())
		{
			const Hypersurface_info& curr_t(q.front());

			const Decorated_triangle_a& t(curr_t.t);

			const Dart_handle& edge(curr_t.edge);

			const unsigned where(curr_t.where);

			if(is_marked_triangle(cm.beta(edge, 2), mk))
			{
				next_q.push(curr_t);

				q.pop();
				continue;				
			}

			const Decorated_triangle_a
				neighbor(get_neighbor_a(t, edge, where));

			const Ordered_triangle
				neighbor_ordered(neighbor.ordered_triangle());
			
			if(already_drawn.find(neighbor_ordered)
			   != already_drawn.end())
			{
				q.pop();
				continue;
			}

			already_drawn.insert(neighbor_ordered);

			
			const Dart_handle sister(cm.beta(edge, 2)),
				nd1(cm.beta(sister, 1)),
				nd2(cm.beta(sister, 0));

			const Hypersurface_info ni1(neighbor, nd1, NEXT),
				ni2(neighbor, nd2, PREV);

			q.push(ni1);
			q.push(ni2);

			hypersurface[hash(sister)].push_back
				(neighbor.to_triangle_s3());

			mark_triangle(sister, mk);

			q.pop();
		}

		cm.negate_mark(mk);

		q = next_q;
		next_q = Q_decorated_triangles_3();
	}

	cm.free_mark(mk);
}

void Computations::monodromy_matrices()
{
	Face_iterator it(cm.one_dart_per_cell<2>().begin()),
		itend(cm.one_dart_per_cell<2>().end());

	for(; it != itend; ++it)
	{
		monodromy_matrices_oriented_edges(it);
		monodromy_matrix(it);
	}
}

void Computations::monodromy_matrices_oriented_edges(const Dart_handle ab)
{
	const Dart_handle bc(cm.beta(ab, 1)),
		ca(cm.beta(ab, 0)),
		ba(cm.beta(ab, 2)),
		cb(cm.beta(bc, 2)),
		ac(cm.beta(ca, 2));
	
	Matrix_33 &m_ab(cm.info(ab).m),
		&m_bc(cm.info(bc).m),
		&m_ca(cm.info(ca).m);

	monodromy_matrix(ab, ba, m_ab);
	monodromy_matrix(bc, cb, m_bc);
	monodromy_matrix(ca, ac, m_ca);
}

void Computations::monodromy_matrix(const Dart_handle& edge,
				    const Dart_handle& sister,
				    Matrix_33& m)
{
	const Expr &q_p(get_coord(cm.info(edge).x)),
		&q_m(get_coord(cm.info(sister).x)),
		inv_q_p(1 / q_p),
		factor(get_cbrt(edge, sister));
	
	m <<    0,  0, q_m,
		0, -1, 0,
		inv_q_p, 0, 0;

	m *= factor;
}

void Computations::monodromy_matrix(const Dart_handle t)
{
	const Expr &t_p(get_coord(cm.info<2>(t).x)),
		inv_factor(get_cbrt(t)),
		factor(1 / inv_factor);

	Matrix_33& m(cm.info<2>(t).m);

	m <<    0, 0, 1,
		0, -1, -1,
		t_p, t_p+1, 1;

	m *= factor;
}

void Computations::get_eigenvalues(const Dart_handle& vertex,
				   Eigenvalues& lambda)
{
	Star_iterator it(cm.one_dart_per_incident_cell<1,0>(vertex).begin()),
		itend(cm.one_dart_per_incident_cell<1,0>(vertex).end());

	Expr X(1), Y(1);
	
	for(; it != itend; ++it)
	{
		const Expr &e_ik(get_coord(cm.info(it).x)),
			&e_ki(get_coord(cm.info(cm.beta(it, 2)).x)),
			&t(get_coord(cm.info<2>(it).x));

		Y *= e_ik;
		X *= e_ki * t;
	}

	const Expr cbrt_X(std::cbrt(to_double(X))),
		cbrt_Y(std::cbrt(to_double(Y))),
		prod_cbrt(cbrt_X * cbrt_Y);
	
	lambda[0] = 1 / (prod_cbrt * cbrt_Y);
	lambda[1] = cbrt_Y / cbrt_X;
	lambda[2] = prod_cbrt * cbrt_X;
}

void Computations::get_all_eigenvalues(Eigenvalues_map& m)
{
	Vertex_iterator it(cm.one_dart_per_cell<0>().begin()),
		itend(cm.one_dart_per_cell<0>().end());

	m.resize(cm.one_dart_per_cell<0>().size());
	
	unsigned long i(0);
	
	for(; it != itend; ++it, ++i)
	{
		Eigenvalues lambda;

		get_eigenvalues(it, lambda);

		cm.info<0>(it) = i;
		m[cm.info<0>(it)] = lambda;
	}
}

void Computations::output_triangulation_file(std::ostream& f)
{
	Dart_handle curr(cm.darts().begin()),
		next;

	Comb_triangle curr_t;

	const unsigned long n_t(cm.one_dart_per_cell<2>().size());
	
	Conc_cm_triangles trgs(n_t);

	unsigned long i(1), return_to(0);
	
	Mark seen(cm.get_new_mark()),
		boundary(cm.get_new_mark());
	
	f << g << " " << n << " " << true << " " << cubic_roots
	  << std::endl << std::endl;

	const Concrete_comb_triangle first_t(curr, curr_t, THIS);

	trgs[0] = first_t;
	mark_triangle(curr, seen);
	mark_comb_edge(cm.beta(curr, 1), boundary);
	mark_comb_edge(cm.beta(curr, 0), boundary);
	
	curr = cm.beta(curr, 2);
	curr_t.adjacent(THIS);
	
	while(i < n_t - 1)
	{
		const Concrete_comb_triangle new_t(curr, curr_t, PREV);

		trgs[i] = new_t;
		
		unsigned& where(trgs[i].where);

		mark_triangle(curr, seen);

		next = cm.beta(curr, 0, 2);
		if(cm.is_marked(next, seen))
		{
			next = cm.beta(curr, 1, 2);
			if(!cm.is_marked(next, seen))
			{
				where = NEXT;
				mark_comb_edge(cm.beta(curr, 0), boundary);
			}
			else
			{
				mark_comb_edge(cm.beta(curr, 0), boundary);
				mark_comb_edge(cm.beta(curr, 1), boundary);
				
				backtrack_poly_build(trgs, return_to, i, curr,
						     curr_t, boundary, seen);
				
				if(curr == null)
					return;
				
				continue;
			}
		}
		else
			mark_comb_edge(cm.beta(curr, 1), boundary);

		
		curr = next;
		curr_t.adjacent(where);

		if(where == THIS || where == PREV)
			return_to = i;

		++i;
	}

	trgs[n_t - 1] = Concrete_comb_triangle(curr, curr_t, PREV);

	mark_comb_edge(cm.beta(curr, 1), boundary);
	mark_comb_edge(cm.beta(curr, 0), boundary);
	
	Boundary bd(N);
	
	build_boundary(trgs, bd, boundary);
	
	for(const Concrete_comb_triangle& t : trgs)
		output_triangle(f, t.dh, t.t);

	f << std::endl;
	
	output_bd_gluing_table(f, bd);
	
	cm.negate_mark(seen);
	cm.free_mark(seen);

	cm.negate_mark(boundary);
	cm.free_mark(boundary);
}


void Computations::output_triangle(std::ostream& f, Dart_handle t,
				   const Comb_triangle& c_t)
{
	f << c_t[THIS] << " " << c_t[NEXT] << " " << c_t[PREV] << " "
	  << get_coord(cm.info(t).a) << " "
	  << get_coord(cm.info(cm.beta(t, 1)).a) << " "
	  << get_coord(cm.info(cm.beta(t, 0)).a) << " "
	  << get_coord(cm.info<2>(t).a)
	  << std::endl;
}

void Computations::output_bd_gluing_table(std::ostream& f, const Boundary& bd)
{
	Mark seen(cm.get_new_mark());
	
	for(unsigned long i(0); i < N; ++i)
	{
		if(cm.is_marked(bd[i], seen))
			continue;
		
		f << hash_dart(bd[i]) << " " << hash_dart(cm.beta(bd[i], 2))
		  << std::endl;

		mark_comb_edge(bd[i], seen);
	}

	cm.negate_mark(seen);
	cm.free_mark(seen);
}


void Computations::unmark_triangle(Dart_handle t, Mark mk)
{
	cm.unmark(t, mk);
	cm.unmark(cm.beta(t, 1), mk);
	cm.unmark(cm.beta(t, 0), mk);
}

void Computations::unmark_edge(Dart_handle edge, Mark mk)
{
	cm.unmark(edge, mk);
}

void Computations::mark_comb_edge(Dart_handle edge, Mark mk)
{
	mark_edge(edge, mk);
	mark_edge(cm.beta(edge, 2), mk);
}

void Computations::unmark_comb_edge(Dart_handle edge, Mark mk)
{
	unmark_edge(edge, mk);
	unmark_edge(cm.beta(edge, 2), mk);
}

void Computations::backtrack_poly_build(Conc_cm_triangles& trgs,
					unsigned long& return_to,
					unsigned long& i, Dart_handle& curr,
					Comb_triangle& curr_t, Mark boundary,
					Mark seen)
{
	while(return_to > 0)
	{
		Concrete_comb_triangle& t(trgs[return_to]);

		if(t.where == NEXT || t.where == NEXT_AND_PREV)
		{
			--return_to;
			continue;
		}

		const Dart_handle next(cm.beta(t.dh, 1, 2));

		if(cm.is_marked(next, seen))
		{
			--return_to;
			continue;
		}

		unmark_comb_edge(next, boundary);
		
		t.where = NEXT_AND_PREV;
		curr = next;
		i = return_to + 1;
		curr_t = t.t;
		curr_t.adjacent(NEXT);
		
		for(; return_to && trgs[return_to].where == NEXT; --return_to);

		return;
	}

	Concrete_comb_triangle& t(trgs[0]);
	i = 1;
	++t.where;
	curr_t = t.t;
	curr_t.adjacent(t.where);

	if(t.where == NEXT)
		curr = cm.beta(t.dh, 1, 2);
	else if(t.where == PREV)
		curr = cm.beta(t.dh, 0, 2);
	else
		curr = null;

	if(curr != null)
		unmark_comb_edge(curr, boundary);
}


void Computations::rearrange_vertices(Conc_cm_triangles& trgs, Boundary& bd,
				      const unsigned long n_t)
{
	unsigned long i, curr_prev(2), curr_next(N - 1);
	
	Permutation sigma(N);

	for(i = 0; i < N; ++i)
		sigma[i] = i;

	bd[0] = cm.beta(trgs[0].dh, 1);
	bd[1] = cm.beta(trgs[0].dh, 0);

	set_hash_dart(bd[0], 0);
	set_hash_dart(bd[1], 1);
	
	for(i = 1; i < n_t - 1; ++i)
	{
		const Concrete_comb_triangle& t(trgs[i]);

		Dart_handle bd_edge;

		if(t.where == PREV)
		{
			bd_edge = cm.beta(t.dh, 1);
			
			bd[curr_prev] = bd_edge;
			set_hash_dart(bd_edge, curr_prev);
			
			sigma[t.t[NEXT]] = curr_prev;
			sigma[t.t[PREV]] = curr_prev == N - 1? 0:curr_prev + 1;
			
			++curr_prev;
		}
		else
		{
			bd_edge = cm.beta(t.dh, 0);
			
			bd[curr_next] = bd_edge;
			set_hash_dart(bd_edge, curr_next);
			
			sigma[t.t[PREV]] = curr_next;
			sigma[t.t[THIS]] = curr_next == N - 1? 0:curr_next + 1;
			
			--curr_next;
		}
	}

	bd[curr_prev] = cm.beta(trgs[n_t - 1].dh, 1);
	bd[curr_next] = cm.beta(trgs[n_t - 1].dh, 0);

	set_hash_dart(bd[curr_next], curr_next);
	set_hash_dart(bd[curr_prev], curr_prev);

	for(Concrete_comb_triangle& t : trgs)
	{
		Comb_triangle& c_t(t.t);

		for(int j(0); j < 3; ++j)
			c_t[j] = sigma[c_t[j]];
	}
}

void Computations::build_boundary(const Conc_cm_triangles& trgs, Boundary& bd,
				  Mark boundary)
{
	for(const Concrete_comb_triangle& t : trgs)
	{
		Dart_handle ab(t.dh), bc(cm.beta(ab, 1)), ca(cm.beta(ab, 0));

		add_to_boundary(ab, bd, t.t[THIS], boundary);
		add_to_boundary(ca, bd, t.t[PREV], boundary);
		add_to_boundary(bc, bd, t.t[NEXT], boundary);
	}
}

void Computations::add_to_boundary(Dart_handle edge, Boundary& bd,
				   const unsigned where, Mark boundary)
{
	if(cm.is_marked(edge, boundary))
	{
		bd[where] = edge;
		set_hash_dart(edge, where);
	}
}

unsigned Computations::relative_pos(Dart_handle edge,
				    const Concrete_comb_triangle& t)
{
	if(edge == t.dh)
		return THIS;
	else if(edge == cm.beta(t.dh, 1))
		return NEXT;
	else
		return PREV;
}


}

