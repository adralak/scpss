#ifndef DRAW_HH
#define DRAW_HH

#include <cairo/cairo.h>
#include <cairo/cairo-svg.h>

#include <cmath>
#include <string>
#include <unistd.h>
#include <random>

#include <CGAL/IO/Color.h>
#include <CGAL/IO/Ostream_iterator.h>
#include <CGAL/Aff_transformation_3.h>
#include <CGAL/aff_transformation_tags.h>

#include "../Tools/tools.hh"
#include "../Decorated_triangles/decorated_triangle.hh"

#include <CGAL/IO/Geomview_stream.h>


namespace SCPSS
{

typedef CGAL::Geomview_stream Base_Geomview;


class Custom_Geomview : public Base_Geomview
{
public:
	typedef CGAL::Bbox_3 Bbox_3;

	
	Custom_Geomview(const Bbox_3 &bbox = Bbox_3(0,0,0, 1,1,1),
			const char *machine = NULL,
			const char *login = NULL) :
		Base_Geomview(bbox, machine, login)
	{
	}

	
	template<class InputIterator>
	void draw_triangles(InputIterator begin, InputIterator end);

        void draw_triangles_on_s3(const Triangles_on_s3& s);
};


class Draw
{
public:
	typedef CGAL::Real_embeddable_traits<Coord>::To_double
	To_double;
	typedef Triangles_3::iterator Tri_3_it;
	typedef CGAL::Bbox_3 Bbox_3;
	typedef CGAL::Color Color;
	typedef std::vector<Color> Colors;
	typedef CGAL::Aff_transformation_3<K> Aff_map_3;
	typedef CGAL::Scaling Scaling;
	typedef CGAL::Translation Translation;
	typedef CGAL::Vector_3<K> CGAL_Vector_3;
	typedef Custom_Geomview Geomview;

	typedef CGAL::Real_embeddable_traits<Expr>::To_double
	To_double_expr;

	
	Draw(Orbits_2& t, const std::string& filename);
	Draw(Orbits_3& orbits);
	Draw(Orbits_s3& orbits);
	Draw(const unsigned long N, const Comb_edges& edges);

private:
	double width = 300, height = 300, l_width = 0.2,
		base_x = width / 2, base_y = height / 2, scale_2 = 70,
		scale_3 = 1, tr_x = 20, tr_y = 20, tr_z = 20,
		scale_s3 = 10, rescale_2 = 1, scale_polygon = 70,
		font_size = 12;
	Bbox_3 window = Bbox_3(0, 0, 0, 50, 50, 50);
	int gv_l_width = 10;
	Color red = Color(255, 0, 0), black = Color(0, 0, 0);
	CGAL_Vector_3 tr = CGAL_Vector_3(tr_x, tr_y, tr_z);
	Aff_map_3 tr_and_scale = Aff_map_3(Translation(), tr)
		* Aff_map_3(Scaling(), scale_3),
		tr_and_scale_s3 = Aff_map_3(Translation(), tr)
		* Aff_map_3(Scaling(), scale_s3);

	const Expr pi = 3.14159265358979323846;
	const To_double_expr to_double = To_double_expr();

	
	void translate_and_scale(Triangles_2& t);
	void translate_and_scale(Point_2& a);
	void translate_and_scale(Triangles_3& t);
	void translate_and_scale(Triangles_on_s3& t);
	void translate_and_scale(Points_3& p);

	void colors(unsigned long n, Colors& c);
	
	void draw_triangle(cairo_t* cr, const Triangle& t);
	void draw_triangle_stroke(cairo_t* cr, const Triangle& t);
	void draw_triangle_fill(cairo_t* cr, const Triangle& t);
	void draw_polygon(const unsigned long N, Points_2& polygon);
};

}



#endif
