#include "draw_2.hh"

namespace SCPSS
{

Draw::Draw(Orbits_2& orbits, const std::string& filename)
{
	cairo_surface_t* surface;
	cairo_t* cr;

	std::string filename_svg(filename),
		filename_png(filename);

	filename_svg.append(".svg");
	filename_png.append(".png");
	
	surface = cairo_svg_surface_create(filename_svg.c_str(), width, height);
	cr = cairo_create(surface);

	cairo_set_line_width(cr, l_width);

	Colors orbit_colors;

	colors(orbits.size(), orbit_colors);

	cairo_set_source_rgb(cr, 0, 0, 0);
	
	for(unsigned j(0); j < orbits.size(); ++j)
	{
		Triangles_2& t(orbits[j]);

		translate_and_scale(t);
		
		for(unsigned i(0); i < t.size(); ++i)
			draw_triangle_stroke(cr, t[i]);
	}

	
	cairo_surface_destroy(surface);
	cairo_destroy(cr);

	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,
					     width,
					     height);
	cr = cairo_create(surface);

	for(unsigned j(0); j < orbits.size(); ++j)
	{
		Triangles_2& t(orbits[j]);
		
		const Color& c(orbit_colors[j]);
		
		cairo_set_source_rgb(cr, c.red(), c.green(), c.blue());
		
		for(unsigned i(0); i < t.size(); ++i)
			draw_triangle_fill(cr, t[i]);
	}

	cairo_surface_write_to_png(surface, filename_png.c_str());
	cairo_surface_destroy(surface);
	cairo_destroy(cr);
}

Draw::Draw(Orbits_3& orbits)
{
	Geomview gv(window);

	Colors orbit_colors;

	colors(orbits.size(), orbit_colors);
	
	for(unsigned i(0); i < orbits.size(); ++i)
	{
		Triangles_3& t(orbits[i]);
		
		translate_and_scale(t);
	
		Tri_3_it it_start(t.begin()),
			it_end(t.end());

		gv.set_bg_color(Color(0, 0, 0));
		gv << orbit_colors[i];
		gv.draw_triangles(it_start, it_end);
	}
	std::cout << "Press a key to exit" << std::endl;

	char c;

	std::cin >> c;
}

Draw::Draw(Orbits_s3& orbits)
{
	Geomview gv(window);

	Colors orbit_colors;

	colors(orbits.size(), orbit_colors);

	for(unsigned i(0); i < orbits.size(); ++i)
	{
		Triangles_on_s3& t(orbits[i]);
		
		translate_and_scale(t);
	
		gv.set_bg_color(Color(0, 0, 0));
		gv << orbit_colors[i];
		gv.draw_triangles_on_s3(t);
	}
	
	std::cout << "Press a key to exit" << std::endl;

	char c;

	std::cin >> c;
}

Draw::Draw(const unsigned long N, const Comb_edges& edges)
{
	if(N == 0)
	{
		std::cout << "Cannot draw 0-gon!" << std::endl;
		return;
	}

	cairo_surface_t* surface;
	cairo_t* cr;

	surface = cairo_svg_surface_create("polygon.svg", width, height);
	cr = cairo_create(surface);

	cairo_set_line_width(cr, l_width);
	cairo_set_source_rgb(cr, 0, 0, 0);

	cairo_select_font_face(cr, "monospace", CAIRO_FONT_SLANT_NORMAL,
			       CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(cr, font_size);
	
	Points_2 polygon;

	draw_polygon(N, polygon);

	for(unsigned long i(0); i < N; ++i)
	{
		const Point_2& p(polygon[i]);
		
		cairo_line_to(cr, p.x, p.y);

		cairo_move_to(cr, p.x + 2, p.y - 3);
		
		std::string point_number(std::to_string(i));

		cairo_show_text(cr, point_number.c_str());

		cairo_move_to(cr, p.x, p.y);
	}

	cairo_line_to(cr, polygon[0].x, polygon[0].y);
	cairo_close_path(cr);
	cairo_stroke(cr);
	
	for(const Comb_edge& e : edges)
	{
		Point_2 a(polygon[e.a]),
			b(polygon[e.b]);

		cairo_move_to(cr, a.x, a.y);
		cairo_line_to(cr, b.x, b.y);
		cairo_close_path(cr);
		cairo_stroke(cr);
	}

	cairo_surface_destroy(surface);
	cairo_destroy(cr);
}


void Draw::translate_and_scale(Triangles_2& t)
{
	for(unsigned i(0); i < t.size(); ++i)
	{
		Triangle& trg(t.at(i));
		
		translate_and_scale(trg.a);
		translate_and_scale(trg.b);
		translate_and_scale(trg.c);		
	}
}

void Draw::translate_and_scale(Point_2& a)
{
	a.x = scale_2 * a.x + base_x;
	a.y = scale_2 * a.y + base_y;
}


void Draw::translate_and_scale(Triangles_3& t)
{
	for(unsigned i(0); i < t.size(); ++i)
	{
		Triangle_3& trg(t.at(i));

		trg = trg.transform(tr_and_scale);
	}
}

void Draw::translate_and_scale(Triangles_on_s3& t)
{
	for(unsigned i(0); i < t.size(); ++i)
	{
		Triangle_on_s3& trg(t.at(i));

		translate_and_scale(trg.edges[0]);
		translate_and_scale(trg.edges[1]);
		translate_and_scale(trg.edges[2]);
	}
}

void Draw::translate_and_scale(Points_3& p)
{
	for(unsigned i(0); i < p.size(); ++i)
	{
		Point_3& pt(p.at(i));

		pt = pt.transform(tr_and_scale_s3);
	}
}

void Draw::colors(unsigned long n, Colors& c)
{
	unsigned long i;
	const unsigned long m(n / 6 + 1);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(1 << 16, (1 << 17) - 1);

	for(i = 0; i < m; ++i)
	{
		unsigned long random_num(distrib(gen));

		const unsigned char r(random_num);

		random_num >>= 8;

		const unsigned char g(random_num);

		random_num >>= 8;

		const unsigned char b(random_num);

		c.push_back(Color(r, g, 0));
		c.push_back(Color(r, 0, b));
		c.push_back(Color(0, g, b));
		c.push_back(Color(r, 0, 0));
		c.push_back(Color(0, g, 0));
		c.push_back(Color(0, 0, b));
	}
}


void Draw::draw_triangle(cairo_t* cr, const Triangle& t)
{
	double ax(t.a.x),
		ay(t.a.y),
		bx(t.b.x),
		by(t.b.y),
		cx(t.c.x),
		cy(t.c.y);
	
	cairo_move_to(cr, ax, ay);
	cairo_line_to(cr, bx, by);
	cairo_line_to(cr, cx, cy);
	cairo_close_path(cr);
}

void Draw::draw_triangle_stroke(cairo_t* cr, const Triangle& t)
{
	draw_triangle(cr, t);
	cairo_stroke(cr);
}

void Draw::draw_triangle_fill(cairo_t* cr, const Triangle& t)
{
	draw_triangle(cr, t);
	cairo_fill(cr);
}

void Draw::draw_polygon(const unsigned long N, Points_2& polygon)
{
	Matrix_22 r;

	Vector_2 v(scale_polygon, 0);

	const Vector_2 tr(base_x, base_y);

	const Expr expr_N(N),
		angle(2 * pi / expr_N),
		cos(std::cos(to_double(angle))),
		sin(std::sin(to_double(angle)));

	r <<    cos, sin,
		-sin, cos;

	for(unsigned long i(0); i < N; ++i)
	{
		const Vector_2 u(v + tr);
		
		Point_2 p(to_double(u(0)), to_double(u(1)));

		polygon.push_back(p);
		v = r * v;
	}
		
}


// Draws a set of triangles as OFF format (it's faster than one by one).
// This is originally a method provided by CGAL
// Slight modification made to be able to color the triangles
template < class InputIterator >
void
Custom_Geomview::draw_triangles(InputIterator begin, InputIterator end)
{
	typedef typename std::iterator_traits<InputIterator>::value_type
		Triangle;
	typedef typename CGAL::Kernel_traits<Triangle>::Kernel           Kernel;
	typedef typename Kernel::Point_3                                  Point;
	typedef typename Kernel::Less_xyz_3                               Comp;

	// We first copy everything in a vector to only require an InputIterator.
	std::vector<Triangle> triangles(begin, end);
	typedef typename std::vector<Triangle>::const_iterator            Tit;

	// Put the points in a map and a vector.
	// The index of a point in the vector is the value associated
	// to it in the map.
	typedef std::map<Point, int, Comp>  Point_map;
	Point_map           point_map(Kernel().less_xyz_3_object());
	std::vector<Point>  points;
	for (Tit i = triangles.begin(); i != triangles.end(); ++i)
		for (int j = 0; j < 3; ++j)
			if (point_map.insert
			    (typename
			     Point_map::value_type(i->vertex(j),
						   points.size())).second)
				points.push_back(i->vertex(j));

	bool ascii_bak = get_ascii_mode();
	bool raw_bak = set_raw(true);

	// Header.
	set_binary_mode();
	(*this) << "(geometry " << get_new_id("triangles")
		<< " {appearance {}{ OFF BINARY\n"
		<< points.size() << triangles.size() << 0;

    
	// Points coordinates.
	std::copy(points.begin(), points.end(),
		  CGAL::Ostream_iterator<Point, Base_Geomview>(*this));

	// Triangles vertices indices.
	for (Tit tit = triangles.begin(); tit != triangles.end(); ++tit) {
		(*this) << 3;
		for (int j = 0; j < 3; ++j)
			(*this) << point_map[tit->vertex(j)];
		(*this) << 4 << fcr() << fcg() << fcb() << 1.0; // WITH color.
	}
	// Footer.
	(*this) << "}})";

	set_raw(raw_bak);
	set_ascii_mode(ascii_bak);
}


void Custom_Geomview::draw_triangles_on_s3(const Triangles_on_s3& t)
{
	typedef Triangles_on_s3::const_iterator Tit;
	
	bool ascii_bak = get_ascii_mode();
	bool raw_bak = set_raw(true);

	const unsigned long S(t[0].S + 1),
		faces_size(3 * S),
		points_size(t.size() * faces_size);

	// Header
	set_binary_mode();
	(*this) << "(geometry " << get_new_id("triangles")
		<< " {appearance {}{ OFF BINARY\n"
		<< points_size << t.size() << 0;

	// Vertices
	for(Tit t_it(t.cbegin()), t_end(t.cend()); t_it != t_end; ++t_it)
	{
		const Triangle_on_s3& trg(*t_it);

		for(int i(0); i < 3; ++i)
		{
			const Points_3& points(trg.edges[i]);
			
			std::copy(points.begin(), points.end(),
				  CGAL::Ostream_iterator<Point_3,
				  Geomview_stream>(*this));
		}

	}

	// Faces
	for (unsigned long t_it(0); t_it < t.size(); ++t_it)
	{
		(*this) << faces_size;
		
		for (int i(0); i < 3; ++i)
		{
			for(int j(0); j < S; ++j)
				(*this) << faces_size * t_it + i * S + j;
		}
		(*this) << 4 << fcr() << fcg() << fcb() << 1.0; // WITH color.
	}
	// Footer
	(*this) << "}})";

	set_raw(raw_bak);
	set_ascii_mode(ascii_bak);
}


}
