#include "app.hh"

namespace SCPSS
{

Application::Application()
{
	Builder_ptr builder(Gtk::Builder::create_from_file("app.glade"));

	builder->get_widget("Window", w);
	builder->get_widget("Cloverleaf_image", im);
	builder->get_widget("Colored_cloverleaf_image", im_c);
	builder->get_widget("New", menu[FILE_NEW]);
	builder->get_widget("Open", menu[FILE_OPEN]);
	builder->get_widget("Save", menu[FILE_SAVE]);
	builder->get_widget("Quit", menu[FILE_QUIT]);
	builder->get_widget("Display_cloverleaf", menu[DISPLAY_CLOVERLEAF]);
	builder->get_widget("Display_r3", menu[DISPLAY_R3]);
	builder->get_widget("Display_s3", menu[DISPLAY_S3]);
	builder->get_widget("Display_cm", menu[DISPLAY_CM]);
	builder->get_widget("Canonical", menu[COMPUTE_CANONICAL]);
	builder->get_widget("Monodromy", menu[COMPUTE_MONODROMY]);
	builder->get_widget("Parameters", menu[TRIANGULATION_PARAMETERS]);
	builder->get_widget("Flip", menu[TRIANGULATION_FLIP]);
	builder->get_widget("Eigenvalues_display", text_disp);
	builder->get_widget("Input_dialog", input);
	builder->get_widget("Input_genus", input_genus);
	builder->get_widget("Input_punctures", input_punctures);
	
	input->set_transient_for(*w);
	input->hide();
	im->hide();
	im_c->hide();

	input->add_button("Submit", Gtk::RESPONSE_OK);
	input->add_button("Cancel", Gtk::RESPONSE_CANCEL);
	
	menu[FILE_NEW]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_new_activate));
	
	menu[FILE_OPEN]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_open_activate));

	menu[FILE_SAVE]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_save_activate));

	menu[FILE_QUIT]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_quit_activate));

	menu[DISPLAY_CLOVERLEAF]->signal_activate().
		connect(sigc::mem_fun(*this,
				      &Application::on_cloverleaf_activate));

	menu[DISPLAY_R3]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_r3_activate));
	
	menu[DISPLAY_S3]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_s3_activate));

	menu[DISPLAY_CM]->signal_activate().
		connect(sigc::mem_fun(*this, &Application::on_cm_activate));

	menu[COMPUTE_CANONICAL]->signal_activate().
		connect(sigc::mem_fun(*this,
				      &Application::on_canonical_activate));

	menu[COMPUTE_MONODROMY]->signal_activate().
		connect(sigc::mem_fun(*this,
				      &Application::on_monodromy_activate));
	
	menu[TRIANGULATION_PARAMETERS]->signal_activate().
		connect(sigc::mem_fun(*this,
				      &Application::on_params_activate));
	
	menu[TRIANGULATION_FLIP]->signal_activate().
		connect(sigc::mem_fun(*this,
				      &Application::on_flip_activate));
}

Application::~Application()
{
	if(c != nullptr)
		delete c;
}


Gtk::Window* Application::get_window_ptr()
{
	return w;
}

void Application::on_new_activate()
{
	input_genus->set_text("");
	input_punctures->set_text("");
	input->show();

	int response(input->run());

	switch(response)
	{
	case Gtk::RESPONSE_OK:
	{
		const unsigned g(get_value(input_genus)),
			n(get_value(input_punctures));

		const unsigned long N(4 * g + 2 * n - 2);

		run_polygon_dialog(g, n, N);

		if(cloverleaf_drawn)
			on_cloverleaf_activate();
		
		break;
	}

	default:
	{
		break;
	}
	
	}

	input->hide();
}


void Application::on_open_activate()
{
	File_chooser fc("Please choose a file",
			Gtk::FILE_CHOOSER_ACTION_OPEN);
	fc.set_transient_for(*w);

	//Add response buttons for the fc:
	fc.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	fc.add_button("Select", Gtk::RESPONSE_OK);

	int response(fc.run());

	//Handle the response:
	switch(response)
	{
	case Gtk::RESPONSE_OK:
	{
		curr_file = fc.get_filename();
		std::ifstream f(curr_file);
		
		im->hide();
		im_c->hide();

		if(c != nullptr)
			delete c;
		
		c = new Computations(f);

		if(cloverleaf_drawn)
			on_cloverleaf_activate();
		
		break;
	}
	case Gtk::RESPONSE_CANCEL:
	{
		break;
	}
	default:
	{
		std::cout << "Unexpected button clicked." << std::endl;
		break;
	}
	}
}


void Application::on_quit_activate()
{
	std::cout << "Quitting..." << std::endl;
	delete w;

	if(c != nullptr)
		delete c;
}

void Application::on_cloverleaf_activate()
{
	if(c == nullptr)
		return;

	cloverleaf_drawn = true;
	
	Orbits_2 cloverleaf;

	std::cout << "Building cloverleaf position..." << std::endl;
	c->build_cloverleaf_position(cloverleaf, 15);
	std::cout << "Done!" << std::endl;

	const std::string clover_filename("cloverleaf");
	
	Draw d(cloverleaf, clover_filename);

	im->set(clover_filename + ".svg");
	im->show();
	im_c->set(clover_filename + ".png");
	im_c->show();
}

void Application::on_r3_activate()
{
	if(c == nullptr)
		return;
	
	Orbits_3 hypersurface;

	std::cout << "Building hypersurface..." << std::endl;
	c->build_hypersurface(hypersurface, 25);
	std::cout << "Done!" << std::endl;

	Draw d(hypersurface);
}

void Application::on_s3_activate()
{
	if(c == nullptr)
		return;
	
	Orbits_s3 hypersurface;

	std::cout << "Building hypersurface..." << std::endl;
	c->build_hypersurface(hypersurface, 25);
	std::cout << "Done!" << std::endl;

	Draw d(hypersurface);
}

void Application::on_canonical_activate()
{
	if(c == nullptr)
		return;

	std::ofstream f(curr_file + "_canonical");
	
	std::cout << "Flipping to canonical cell decomposition..." << std::endl;
	c->canonical_cell_decomposition(f);
	std::cout << "Done!" << std::endl;

	std::string canonical_filename
		(choose_file_name(curr_file,
				  "Choose a file if you wish to save"
				  " the triangulation to a file"));

	if(canonical_filename != "")
	{
		std::ofstream canonical_file(canonical_filename);
		
		c->output_triangulation_file(canonical_file);
	}
	
	if(cloverleaf_drawn)
		on_cloverleaf_activate();
}

void Application::on_monodromy_activate()
{
	if(c == nullptr)
		return;

	std::string matrices_filename
		(choose_file_name(curr_file,
				  "Choose a file to write the matrices to"));

	if(matrices_filename == "")
		return;
	
	std::ofstream f(matrices_filename);
	
	std::cout << "Writing monodromy matrices to file..." << std::endl;
	c->output_monodromy_matrices(f);
	std::cout << "Done!" << std::endl;
}


void Application::send_to_buffer(const Eigenvalues_map& m)
{
	ustring all_text;

	for(unsigned long i(0); i < m.size(); ++i)
	{
		const Eigenvalues& e(m[i]);

		const double l0(to_double(e[0])),
			l1(to_double(e[1])),
			l2(to_double(e[2]));

		std::string s_i(std::to_string(i)),
			s_l0(std::to_string(l0)),
			s_l1(std::to_string(l1)),
			s_l2(std::to_string(l2)),
			curr_line(s_i + ": " + s_l0 + "; " + s_l1 + "; " + s_l2
				  + "\n");

		ustring ucurr_line(curr_line);
		
		all_text.append(ucurr_line);
	}

	Text_buffer_ptr buff_ptr(text_disp->get_buffer());

	buff_ptr->set_text(all_text);

	text_disp->set_buffer(buff_ptr);
}


void Application::run_polygon_dialog(const unsigned g, const unsigned n,
				     const unsigned long N)
{
	Builder_ptr builder
		(Gtk::Builder::create_from_file("polygon_dialog.glade"));

	Dialog* poly_dialog;
	Text_view* poly_edges;
	Image* poly_img;

	builder->get_widget("Polygon_dialog", poly_dialog);
	builder->get_widget("Polygon_edges", poly_edges);
	builder->get_widget("Polygon_img", poly_img);

	poly_dialog->set_transient_for(*w);
	poly_dialog->add_button("Preview", PREVIEW);
	poly_dialog->add_button("Cone", CONE);
	poly_dialog->add_button("Zigzag", ZIGZAG);
	poly_dialog->add_button("Submit", Gtk::RESPONSE_ACCEPT);
	poly_dialog->add_button("Cancel", Gtk::RESPONSE_CANCEL);

	bool run(true);

	Comb_edges empty_edges;
	
	Draw empty_polygon(N, empty_edges);

	poly_img->set("polygon.svg");
	
	while(run)
	{
		int response(poly_dialog->run());

		switch(response)
		{
		case PREVIEW:
		{
			Comb_edges edges;
			get_edges(poly_edges, edges);

			Draw polygon(N, edges);
			poly_img->set("polygon.svg");

			break;
		}

		case Gtk::RESPONSE_ACCEPT:
		{
			Comb_edges edges;
			get_edges(poly_edges, edges);

			if(edges.size() != N - 3)
			{
				std::cout << "There should be " << N - 3
					  << " edges!" << std::endl;
				break;
			}
			
			if(c != nullptr)
				delete c;

			std::string new_filename(choose_file_name
						 ("s_" + std::to_string(g)
						  + "_" + std::to_string(n),
						  "Please choose a file to"
						  " save the triangulation"
						  " to"));

			if(new_filename == "")
				break;
			
			std::ofstream new_file(new_filename);

			c = new Computations(g, n, edges, new_filename);

			run = false;			
			break;
		}

		case CONE:
		{
			const unsigned long vertex(run_vertex_dialog(N));

			if(vertex == N)
				break;

			write_cone(vertex, N, poly_edges);
			poly_img->set("polygon.svg");
				
			break;
		}

		case ZIGZAG:
		{
			const unsigned long vertex(run_vertex_dialog(N));

			if(vertex == N)
				break;

			Comb_edges edges;
			
			write_zigzag(vertex, N, poly_edges);
			poly_img->set("polygon.svg");
			
			break;
		}
		
		default:
		{
			run = false;
			break;
		}
		
		}
	}

	delete poly_dialog;
}


void Application::get_edges(Text_view* poly_edges, Comb_edges& edges)
{
	Text_buffer_ptr buff_ptr(poly_edges->get_buffer());

	ustring u_text_edges(buff_ptr->get_text());

	std::string text_edges(u_text_edges.raw());

	std::stringstream edges_stream(text_edges,
				       std::ios_base::in);
			
	unsigned a, b;

	while(edges_stream >> a >> b)
	{
		Comb_edge e(a, b);

		edges.push_back(e);
	}
}


void Application::on_cm_activate()
{
	if(c == nullptr)
		return;
	
	Text_buffer_ptr buff_ptr(text_disp->get_buffer());

	std::stringstream specs_stream(std::ios_base::out);

	c->show_specs(specs_stream);

	const ustring u_specs(specs_stream.str());

	buff_ptr->set_text(u_specs);
}


unsigned long Application::run_vertex_dialog(const unsigned long N)
{
	Builder_ptr d_builder
		(Gtk::Builder::create_from_file
		 ("vertex_dialog.glade"));

	Dialog* vertex_dialog;
	Entry* vertex_entry;

	d_builder->get_widget("Vertex_dialog", vertex_dialog);
	d_builder->get_widget("Vertex_entry", vertex_entry);

	vertex_dialog->set_transient_for(*w);
	vertex_dialog->add_button("Submit", Gtk::RESPONSE_ACCEPT);
	vertex_dialog->add_button("Cancel", Gtk::RESPONSE_CANCEL);

	int response(vertex_dialog->run());

	unsigned long vertex(N);
	
	switch(response)
	{
	case Gtk::RESPONSE_ACCEPT:
	{
		vertex = get_value(vertex_entry);
		break;
	}

	default:
		break;
	}

	delete vertex_dialog;
	
	return vertex;
}


unsigned long Application::get_value(const Entry* e)
{
	const ustring u_entry(e->get_text());

	const std::string entry(u_entry.raw());

	return std::stoul(entry);
}


void Application::write_cone(const unsigned long v, const unsigned long N,
			     Text_view* write_to)
{
	unsigned long curr_next(v);

	Text_buffer_ptr buff_ptr(write_to->get_buffer());

	std::stringstream edges_stream(std::ios_base::out);

	Comb_edges edges;
	
	n_incr_mod(curr_next, N, 2);

	for(unsigned long i(0); i < N - 3; ++i, incr_mod(curr_next, N))
	{
		edges_stream << v << " " << curr_next << std::endl;

		Comb_edge e(v, curr_next);
		edges.push_back(e);
	}

	ustring u_edges_text(edges_stream.str());

	buff_ptr->set_text(u_edges_text);
	
	Draw polygon(N, edges);
}

void Application::write_zigzag(const unsigned long v, const unsigned long N,
			       Text_view* write_to)
{
	unsigned long curr_next(v), curr_prev(v);

	bool next(false);
	
	Text_buffer_ptr buff_ptr(write_to->get_buffer());

	std::stringstream edges_stream(std::ios_base::out);

	Comb_edges edges;

	n_incr_mod(curr_next, N, 2);

	for(unsigned long i(0); i < N - 3; ++i, next = !next)
	{
		Comb_edge e(curr_prev, curr_next);
		edges.push_back(e);

		if(next)
		{
			edges_stream << curr_next << " " << curr_prev
				     << std::endl;

			incr_mod(curr_next, N);
		}
		else
		{
			edges_stream << curr_prev << " " << curr_next
				     << std::endl;

			decr_mod(curr_prev, N);
		}
	}
	
	ustring u_edges_text(edges_stream.str());

	buff_ptr->set_text(u_edges_text);
	
	Draw polygon(N, edges);
}

void Application::on_params_activate()
{
	if(c == nullptr)
		return;
	
	Builder_ptr builder
		(Gtk::Builder::create_from_file("parameter_dialog.glade"));

	Dialog* param_dialog;
	Text_view* params;
	Label* gluing_table;

	builder->get_widget("Param_dialog", param_dialog);
	builder->get_widget("Param_gluing_table", gluing_table);
	builder->get_widget("Param_input", params);

	param_dialog->set_transient_for(*w);
	param_dialog->add_button("Submit", Gtk::RESPONSE_ACCEPT);
	param_dialog->add_button("Cancel", Gtk::RESPONSE_CANCEL);

	Text_buffer_ptr buff_ptr(params->get_buffer());

	std::stringstream gluing_table_stream(std::ios_base::out),
		a_coords_stream(std::ios_base::out);

	Face_range f_r(c->output_gluing_table(gluing_table_stream));

	c->output_a_coords(a_coords_stream, f_r);
	
	ustring u_gluing_table(gluing_table_stream.str()),
		u_a_coords(a_coords_stream.str());

	buff_ptr->set_text(u_a_coords);
	gluing_table->set_text(u_gluing_table);

	bool run(true);

	while(run)
	{
		int response(param_dialog->run());

		switch(response)
		{
		case Gtk::RESPONSE_ACCEPT:
		{
			ustring u_text_params(buff_ptr->get_text());

			std::string text_params(u_text_params.raw());

			std::stringstream params_stream(text_params,
							std::ios_base::in);

			A_coords ac;
		
			get_a_coords(params_stream, ac);

			if(ac.size() != 4 * f_r.size())
			{
				std::cout << "Expected " << 4 * f_r.size()
					  << " coordinates; got " << ac.size()
					  << std::endl;
				break;
			}
		
			c->set_a_coords(ac, f_r);

			if(cloverleaf_drawn)
				on_cloverleaf_activate();
			
			run = false;
			break;
		}

		default:
			run = false;
			break;
		}
	}
	delete param_dialog;
}

void Application::get_a_coords(std::istream& in, A_coords& ac)
{
	Coord c;

	while(in >> c)
		ac.push_back(c);
}



void Application::on_flip_activate()
{
	if(c == nullptr)
		return;
	
	Builder_ptr builder
		(Gtk::Builder::create_from_file("flip_dialog.glade"));

	Dialog* flip_dialog;
	Entry* to_flip_input;
	Label* gluing_table;

	builder->get_widget("Flip_dialog", flip_dialog);
	builder->get_widget("Flip_gluing_table", gluing_table);
	builder->get_widget("Flip_input", to_flip_input);

	flip_dialog->set_transient_for(*w);
	flip_dialog->add_button("Submit", Gtk::RESPONSE_ACCEPT);
	flip_dialog->add_button("Cancel", Gtk::RESPONSE_CANCEL);
	

	std::stringstream gluing_table_stream(std::ios_base::out);

	Face_range f_r(c->output_gluing_table(gluing_table_stream));

	ustring u_gluing_table(gluing_table_stream.str());

	gluing_table->set_text(u_gluing_table);
	
	int response(flip_dialog->run());

	switch(response)
	{
	case Gtk::RESPONSE_ACCEPT:
	{
		const ustring u_to_flip(to_flip_input->get_text());

		const std::string to_flip_str(u_to_flip.raw());

		std::stringstream to_flip_stream(to_flip_str,
						 std::ios_base::in);

		unsigned long trg;
		
		unsigned edge;

		to_flip_stream >> trg >> edge;

		c->flip(trg, edge);
		
		if(cloverleaf_drawn)
			on_cloverleaf_activate();
		
		break;
	}

	default:
		break;
	}

	delete flip_dialog;
}

std::string Application::choose_file_name(const std::string& default_name,
					  const std::string& message)
{
	File_chooser fc(message, Gtk::FILE_CHOOSER_ACTION_SAVE);
	fc.set_transient_for(*w);

	//Add response buttons the the fc:
	fc.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	fc.add_button("Select", Gtk::RESPONSE_OK);

	fc.set_filename(default_name);
	
	int response(fc.run());

	std::string file_name;
	
	switch(response)
	{
	case Gtk::RESPONSE_OK:
	{
		file_name = fc.get_filename();
		break;
	}
	default:
		file_name = "";
		break;
	}

	return file_name;
}

void Application::on_save_activate()
{
	Path from, to;

	if(cloverleaf_drawn)
	{
		std::string filename
			(choose_file_name("cloverleaf",
					  "Please choose a base"
					  " name for the cloverleaf files")),
			filename_svg(filename + ".svg"),
			filename_png(filename + ".png");

		to = filename_svg.c_str();
		from = "cloverleaf.svg";
		from = boost::filesystem::canonical(from);
		boost::filesystem::copy_file
			(from, to,
			 boost::filesystem::copy_option::overwrite_if_exists);

		to = filename_png.c_str();
		from = "cloverleaf.png";
		from = boost::filesystem::canonical(from);
		boost::filesystem::copy_file
			(from, to,
			 boost::filesystem::copy_option::overwrite_if_exists);
	}

	std::string new_filename(choose_file_name
				 ("s_g_n",
				  "Please choose a file to"
				  " save the triangulation"
				  " to"));
	
	std::ofstream f(new_filename);

	c->output_triangulation_file(f);
}

}
