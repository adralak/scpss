#ifndef APP_HH
#define APP_HH

#include <gtkmm.h>
#include <sstream>

#include <boost/filesystem.hpp>

#include "../Computations/computations.hh"
#include "../Draw/draw_2.hh"


namespace SCPSS
{

class Application
{
public:
	typedef Gtk::Window Window;
	typedef Gtk::Image Image;
	typedef Gtk::MenuItem Menu_item;
	typedef Glib::RefPtr<Gtk::Builder> Builder_ptr;
	typedef Gtk::FileChooserDialog File_chooser;
	typedef Gtk::TextView Text_view;
	typedef Glib::ustring ustring;
	typedef Gtk::TextBuffer Text_buffer;
	typedef Glib::RefPtr<Text_buffer> Text_buffer_ptr;
	typedef Gtk::Button Button;
	typedef Gtk::Entry Entry;
	typedef Gtk::Label Label;
	typedef Gtk::Dialog Dialog;

	typedef boost::filesystem::path Path;
	
	typedef Tools::Face_range Face_range;
	
	enum MENU_ITEM
	{
		FILE_NEW = 0,
		FILE_OPEN,
		FILE_SAVE,
		FILE_QUIT,
		DISPLAY_CLOVERLEAF,
		DISPLAY_R3,
		DISPLAY_S3,
		DISPLAY_CM,
		COMPUTE_CANONICAL,
		COMPUTE_MONODROMY,
		TRIANGULATION_PARAMETERS,
		TRIANGULATION_FLIP,
		MENU_ITEM_TOTAL
	};

	enum RESPONSES
	{
		PREVIEW = 0,
		CONE,
		ZIGZAG
	};
	
	Application();

	~Application();
	
	Window* get_window_ptr();
	
private:
	Computations *c = nullptr;
	Menu_item* menu[MENU_ITEM_TOTAL];
	Window *w;
	Image *im, *im_c;
	Text_view *text_disp;
	std::string curr_file;
	Dialog *input;
	Entry *input_genus, *input_punctures;
	bool cloverleaf_drawn = false;
	
	const Computations::To_double_expr to_double =
		Computations::To_double_expr();

	void on_new_activate();
	
	void on_open_activate();

	void on_quit_activate();

	void on_cloverleaf_activate();

	void on_r3_activate();

	void on_s3_activate();

	void on_canonical_activate();

	void on_monodromy_activate();

	void send_to_buffer(const Eigenvalues_map& m);

	void run_polygon_dialog(const unsigned g, const unsigned n,
				const unsigned long N);

	void get_edges(Text_view* poly_edges, Comb_edges& edges);

	void on_cm_activate();

	unsigned long run_vertex_dialog(const unsigned long N);

	unsigned long get_value(const Entry* e);

	void write_cone(const unsigned long v, const unsigned long N,
			Text_view* write_to);
	
	void write_zigzag(const unsigned long v, const unsigned long N,
			  Text_view* write_to);
		
	void incr_mod(unsigned long& a, const unsigned long N)
	{
		a = a + 1 == N? 0 : a + 1;
	}

	void n_incr_mod(unsigned long& a, const unsigned long N,
			const unsigned long n)
	{
		a = a + n >= N? a + n - N : a + n;
	}

	void decr_mod(unsigned long& a, const unsigned long N)
	{
		a = a? a - 1 : N - 1;
	}

	void on_params_activate();

	void get_a_coords(std::istream& in, A_coords& ac);
	
	void on_flip_activate();

	std::string choose_file_name(const std::string& default_name,
				     const std::string& message);

	void on_save_activate();
};

}

#endif
