#ifndef DECORATED_TRIANGLE_HH
#define DECORATED_TRIANGLE_HH

#include "../Tools/tools.hh"

namespace SCPSS
{

struct Point_2
{
	double x, y;

	Point_2()
	{
	}

	Point_2(double x, double y) : x(x), y(y)
	{
	}
};

struct Triangle
{
	Point_2 a, b, c;

	Triangle()
	{
	}

	Triangle(const Point_2& a, const Point_2& b, const Point_2& c)
		: a(a), b(b), c(c)
	{
	}
};

typedef std::vector<Triangle> Triangles_2;
typedef std::vector<Triangles_2> Orbits_2;
typedef std::vector<Point_2> Points_2;

struct Decoration
{
	Vector_3 c;
	Line e;

	Decoration()
	{
	}
	
	Decoration(const Vector_3& c, const Line& e) : c(c), e(e)
	{
	}

	Decoration(const Decoration& d) = default;
	
	Decoration& operator=(const Decoration& d) = default;
};

enum
{
	THIS = 0,
	NEXT,
	PREV,
	NEXT_AND_PREV
};

struct Triangle_on_s3
{
	Points_3 edges[3];
	typedef CGAL::Real_embeddable_traits<Expr>::To_double
	To_double_expr;

	static constexpr To_double_expr to_double = To_double_expr();
	static constexpr unsigned long S = 16;
	
	Triangle_on_s3();

	Triangle_on_s3(const Vector_3& a, const Vector_3& b, const Vector_3& c);

	
private:
	void make_edge(int i, const Vector_3& begin, const Vector_3& end);

	Point_3 stereo_proj(const Vector_4d& v);
	
};

typedef std::vector<Triangle_on_s3> Triangles_on_s3;
typedef std::vector<Triangles_on_s3> Orbits_s3;


struct __Point_3
{
	Expr x, y, z;

	__Point_3()
	{
	}

	__Point_3(const Vector_3& v) : x(v(0)), y(v(1)), z(v(2))
	{
	}

	bool operator<(const __Point_3& p) const
	{
		return lexico(x, y, z, p.x, p.y, p.z);
	}
};

struct Ordered_triangle
{
	__Point_3 a, b, c;

	Ordered_triangle()
	{
	}

	Ordered_triangle(const __Point_3& _a, const __Point_3& _b,
			 const __Point_3& _c) : a(_a), b(_b), c(_c)
	{
		sort(a, b, c);
	}
		
	
	bool operator<(const Ordered_triangle& t) const
	{
		return lexico(a, b, c, t.a, t.b, t.c);
	}

};

typedef std::set<Ordered_triangle> Ordered_triangles;


struct Decorated_triangle
{
	Decoration d[3];
	
	typedef CGAL::Real_embeddable_traits<Expr>::To_double
	To_double_expr;

	static constexpr To_double_expr to_double_dt = To_double_expr();

	Decorated_triangle();
	
	Decorated_triangle(const Decoration& d0, const Decoration& d1,
			   const Decoration& d2);


	Decorated_triangle(const Decorated_triangle& t, const Decoration& new_d,
			   const unsigned where);

	
	Decorated_triangle& operator=(const Decorated_triangle& t)
	= default;
	
	Decoration& operator[](unsigned i);

	const Decoration& operator[](unsigned i) const;

	Ordered_triangle ordered_triangle() const;
};

struct Decorated_triangle_a : public Decorated_triangle
{
	Decorated_triangle_a();

	Decorated_triangle_a(const Decoration& d0, const Decoration& d1,
			     const Decoration& d2);

	Decorated_triangle_a(const Decorated_triangle& t, const Decoration& new_d,
			     const unsigned where);

	
	Triangle_3 to_triangle3() const;
	
	Triangle_on_s3 to_triangle_s3() const;
};


struct Decorated_triangle_x : public Decorated_triangle
{
	Line edges[3];

	Decorated_triangle_x();
	
	Decorated_triangle_x(const Decoration& d0, const Decoration& d1,
			     const Decoration& d2);

	Decorated_triangle_x(const Decorated_triangle_x& t,
			     const Decoration& new_d,
			     const Line& e1, const Line& e2,
			     const int where);

	Decorated_triangle_x(const Decorated_triangle_x& t);

	Decorated_triangle_x& operator=(const Decorated_triangle_x& t)
	= default;
	
	
	Line& operator()(unsigned i);

	const Line& operator()(unsigned i) const;

private:
	void get_edge(int i);
};

}

#endif
