#include "decorated_triangle.hh"

namespace SCPSS
{

Decorated_triangle::Decorated_triangle()
{
}
	
Decorated_triangle::Decorated_triangle(const Decoration& d0,
				       const Decoration& d1,
				       const Decoration& d2)
{
	d[0] = d0;
	d[1] = d1;
	d[2] = d2;
}

Decorated_triangle::Decorated_triangle(const Decorated_triangle& t,
				       const Decoration& new_d,
				       const unsigned where)
{
	d[0] = t[where + 1];
	d[1] = t[where];
	d[2] = new_d;
}
	
Decoration& Decorated_triangle::operator[](unsigned i)
{
	return d[i % 3];
}

const Decoration& Decorated_triangle::operator[](unsigned i) const
{
	return d[i % 3];
}

Ordered_triangle Decorated_triangle::ordered_triangle() const
{
	return Ordered_triangle(d[0].c, d[1].c, d[2].c);
}

Decorated_triangle_a::Decorated_triangle_a() : Decorated_triangle()
{
}

Decorated_triangle_a::Decorated_triangle_a(const Decoration& d0,
					   const Decoration& d1,
					   const Decoration& d2) :
	Decorated_triangle(d0, d1, d2)
{
}

Decorated_triangle_a::Decorated_triangle_a(const Decorated_triangle& t,
					   const Decoration& new_d,
					   const unsigned where) :
	Decorated_triangle(t, new_d, where)
{
}

	
Triangle_3 Decorated_triangle_a::to_triangle3() const
{
	static Matrix_44 P;
		
	P <<     1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1;

		
	const Vector_3 &a(d[0].c),
		&b(d[1].c),
		&c(d[2].c);

	const Vector_4 a_proj(a(0), a(1), a(2), 1),
		b_proj(b(0), b(1), b(2), 1),
		c_proj(c(0), c(1), c(2), 1);
		
	Vector_4 new_a(P * a_proj),
		new_b(P * b_proj),
		new_c(P * c_proj);
	        
	new_a /= new_a(3);
	new_b /= new_b(3);
	new_c /= new_c(3);
		
	const double a0(to_double_dt(new_a(0))),
		a1(to_double_dt(new_a(1))),
		a2(to_double_dt(new_a(2))),
		b0(to_double_dt(new_b(0))),
		b1(to_double_dt(new_b(1))),
		b2(to_double_dt(new_b(2))),
		c0(to_double_dt(new_c(0))),
		c1(to_double_dt(new_c(1))),
		c2(to_double_dt(new_c(2)));

	const Point_3 A(a0, a1, a2),
		B(b0, b1, b2),
		C(c0, c1, c2);

	const Triangle_3 ABC(A, B, C);

	return ABC;
}

Triangle_on_s3 Decorated_triangle_a::to_triangle_s3() const
{
	const Vector_3 &a(d[0].c),
		&b(d[1].c),
		&c(d[2].c);

	const Triangle_on_s3 t(a, b, c);
		
	return t;
}



Decorated_triangle_x::Decorated_triangle_x()
{
}
	
Decorated_triangle_x::Decorated_triangle_x(const Decoration& d0,
					   const Decoration& d1,
					   const Decoration& d2)
{
	d[0] = d0;
	d[1] = d1;
	d[2] = d2;

	for(int i(0); i < 3; ++i)
		get_edge(i);
}

Decorated_triangle_x::Decorated_triangle_x(const Decorated_triangle_x& t,
					   const Decoration& new_d,
					   const Line& e1, const Line& e2,
					   const int where)
{
	d[0] = t[where + 1];
	d[1] = t[where];
	d[2] = new_d;

	edges[0] = t(where);
	edges[1] = e1;
	edges[2] = e2;
}


Decorated_triangle_x::Decorated_triangle_x(const Decorated_triangle_x& t)
{
	d[0] = t[0];
	d[1] = t[1];
	d[2] = t[2];

	edges[0] = t(0);
	edges[1] = t(1);
	edges[2] = t(2);
}
	
	
Line& Decorated_triangle_x::operator()(unsigned i)
{
	return edges[i % 3];
}

const Line& Decorated_triangle_x::operator()(unsigned i) const
{
	return edges[i % 3];
}

	
void Decorated_triangle_x::get_edge(int i)
{
	Matrix_33 m;

	const Vector_3& A(d[i].c),
		&B(d[(i + 1) % 3].c);

	const Line At(A.transpose()),
		Bt(B.transpose());
		
	m << At, Bt, At + Bt;

	Full_LU_3 lu_m(m);

	const Vector_3& k(lu_m.kernel().col(0));
		
	const Line line(k.transpose());

	edges[i] = line;
}

Triangle_on_s3::Triangle_on_s3()
{
}

Triangle_on_s3::Triangle_on_s3(const Vector_3& a,
			       const Vector_3& b, const Vector_3& c)
{
	make_edge(0, a, b);
	make_edge(1, b, c);
	make_edge(2, c, a);
}
	
	
void Triangle_on_s3::make_edge(int i, const Vector_3& begin,
			       const Vector_3& end)
{
	const double a0(to_double(begin(0))),
		a1(to_double(begin(1))),
		a2(to_double(begin(2))),
		b0(to_double(end(0))),
		b1(to_double(end(1))),
		b2(to_double(end(2)));

	Vector_4d a(a0, a1, a2, 1),
		b(b0, b1, b2, 1),
		c;

	a /= a.stableNorm();
	b /= b.stableNorm();

	edges[i].push_back(stereo_proj(a));
		
	for(unsigned s(1); s < S; ++s)
	{
		c = s * b + (S - s) * a;
		c /= c.stableNorm();
			
		edges[i].push_back(stereo_proj(c));
	}

	edges[i].push_back(stereo_proj(b));
}

Point_3 Triangle_on_s3::stereo_proj(const Vector_4d& v)
{
	static const double no_zeroes(1.000001);

	const double scale(1 / (no_zeroes + v(0))),
		p0(v(1) * scale),
		p1(v(2) * scale),
		p2(v(3) * scale);

	Point_3 p(p0, p1, p2);

	return p;
}


}
